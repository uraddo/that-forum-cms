﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.DAL.Entities;
using ThatForumCMS.DAL.Entities.Identity;
using ThatForumCMS.DAL.Infrastructure;
using ThatForumCMS.DAL.Repositories;

namespace ThatForumCMS.DAL.Interfaces {
    public interface IForumUnitOfWork : IDisposable {
        IGenericRepository<UserProfile> UserProfileRepository { get; }
        IGenericRepository<Topic> TopicRepository { get;}
        IGenericRepository<Category> CategoryRepository { get; }
        IGenericRepository<Post> PostRepository { get; }
        IGenericRepository<Tag> TagRepository { get; }
        IGenericRepository<ErrorLog> ErrorLogRepository { get; }
        IGenericRepository<UserProfileAbout> UserProfileAboutRepository { get; }
        IGenericRepository<UserProfileSignature> UserProfileSignatureRepository { get; }

        Task SaveAsync();
        void Save();

    }
}
