﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.DAL.Identity;

namespace ThatForumCMS.DAL.Interfaces {
    
    public interface IIdentityUnitOfWork : IDisposable {
        ApplicationUserManager UserManager { get; }
        //IUserProfileManager UserProfileManager { get; }
        ApplicationRoleManager RoleManager { get; }
        Task SaveAsync();

    }
}
