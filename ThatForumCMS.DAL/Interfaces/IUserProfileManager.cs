﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.DAL.Entities.Identity;

namespace ThatForumCMS.DAL.Interfaces {
    public interface IUserProfileManager : IDisposable {
        void Create(UserProfile item);
        void Delete(UserProfile profileToDelete);
        void Update(UserProfile item);
        UserProfile GetById(string id);
        //UserProfile GetByLogin(string login);
        UserProfile GetByApplicationUserId(string id);
    }
}
