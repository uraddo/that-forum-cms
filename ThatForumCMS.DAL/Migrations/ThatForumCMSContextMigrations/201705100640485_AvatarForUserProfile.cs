namespace ThatForumCMS.DAL.Migrations.ThatForumCMSContextMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AvatarForUserProfile : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfiles", "AvatarUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserProfiles", "AvatarUrl");
        }
    }
}
