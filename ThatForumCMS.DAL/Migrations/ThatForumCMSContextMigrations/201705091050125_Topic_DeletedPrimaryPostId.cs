namespace ThatForumCMS.DAL.Migrations.ThatForumCMSContextMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Topic_DeletedPrimaryPostId : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Topics", "PrimaryPostId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Topics", "PrimaryPostId", c => c.Int(nullable: false));
        }
    }
}
