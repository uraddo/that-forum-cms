namespace ThatForumCMS.DAL.Migrations.ThatForumCMSContextMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SomethingChanged : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Topics", "CreationDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Topics", "CreationDate");
        }
    }
}
