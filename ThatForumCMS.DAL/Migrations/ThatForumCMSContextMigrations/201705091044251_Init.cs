namespace ThatForumCMS.DAL.Migrations.ThatForumCMSContextMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ErrorLogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Priority = c.Byte(nullable: false),
                        CmsMessage = c.String(nullable: false),
                        ExceptionMsg = c.String(),
                        ExceptionType = c.String(),
                        ExceptionSource = c.String(),
                        ExceptionURL = c.String(),
                        Logdate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        ParentCategoryId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.ParentCategoryId)
                .Index(t => t.ParentCategoryId);
            
            CreateTable(
                "dbo.Topics",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoryId = c.Int(nullable: false),
                        UserProfileId = c.Int(),
                        Title = c.String(nullable: false),
                        ViewsCount = c.Int(nullable: false),
                        IsPinned = c.Boolean(nullable: false),
                        TopicState = c.Int(nullable: false),
                        PrimaryPostId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.UserProfiles", t => t.UserProfileId)
                .Index(t => t.CategoryId)
                .Index(t => t.UserProfileId);
            
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserProfileId = c.Int(nullable: false),
                        Message = c.String(nullable: false),
                        PostTime = c.DateTime(nullable: false),
                        PostStatus = c.Int(nullable: false),
                        TopicId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Topics", t => t.TopicId, cascadeDelete: true)
                .ForeignKey("dbo.UserProfiles", t => t.UserProfileId, cascadeDelete: true)
                .Index(t => t.UserProfileId)
                .Index(t => t.TopicId);
            
            CreateTable(
                "dbo.UserProfiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApplicationUserId = c.String(nullable: false),
                        RealName = c.String(),
                        AccountCreationDate = c.DateTime(nullable: false),
                        LastVisit = c.DateTime(nullable: false),
                        ProfileHidden = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserProfileAbouts",
                c => new
                    {
                        UserProfileAboutId = c.Int(nullable: false),
                        About = c.String(nullable: false, maxLength: 800),
                    })
                .PrimaryKey(t => t.UserProfileAboutId)
                .ForeignKey("dbo.UserProfiles", t => t.UserProfileAboutId)
                .Index(t => t.UserProfileAboutId);
            
            CreateTable(
                "dbo.UserProfileSignatures",
                c => new
                    {
                        UserProfileSignatureId = c.Int(nullable: false),
                        Signature = c.String(nullable: false, maxLength: 300),
                    })
                .PrimaryKey(t => t.UserProfileSignatureId)
                .ForeignKey("dbo.UserProfiles", t => t.UserProfileSignatureId)
                .Index(t => t.UserProfileSignatureId);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TagTopics",
                c => new
                    {
                        Tag_Id = c.Int(nullable: false),
                        Topic_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tag_Id, t.Topic_Id })
                .ForeignKey("dbo.Tags", t => t.Tag_Id, cascadeDelete: true)
                .ForeignKey("dbo.Topics", t => t.Topic_Id, cascadeDelete: true)
                .Index(t => t.Tag_Id)
                .Index(t => t.Topic_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TagTopics", "Topic_Id", "dbo.Topics");
            DropForeignKey("dbo.TagTopics", "Tag_Id", "dbo.Tags");
            DropForeignKey("dbo.UserProfileSignatures", "UserProfileSignatureId", "dbo.UserProfiles");
            DropForeignKey("dbo.UserProfileAbouts", "UserProfileAboutId", "dbo.UserProfiles");
            DropForeignKey("dbo.Topics", "UserProfileId", "dbo.UserProfiles");
            DropForeignKey("dbo.Posts", "UserProfileId", "dbo.UserProfiles");
            DropForeignKey("dbo.Posts", "TopicId", "dbo.Topics");
            DropForeignKey("dbo.Topics", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.Categories", "ParentCategoryId", "dbo.Categories");
            DropIndex("dbo.TagTopics", new[] { "Topic_Id" });
            DropIndex("dbo.TagTopics", new[] { "Tag_Id" });
            DropIndex("dbo.UserProfileSignatures", new[] { "UserProfileSignatureId" });
            DropIndex("dbo.UserProfileAbouts", new[] { "UserProfileAboutId" });
            DropIndex("dbo.Posts", new[] { "TopicId" });
            DropIndex("dbo.Posts", new[] { "UserProfileId" });
            DropIndex("dbo.Topics", new[] { "UserProfileId" });
            DropIndex("dbo.Topics", new[] { "CategoryId" });
            DropIndex("dbo.Categories", new[] { "ParentCategoryId" });
            DropTable("dbo.TagTopics");
            DropTable("dbo.Tags");
            DropTable("dbo.UserProfileSignatures");
            DropTable("dbo.UserProfileAbouts");
            DropTable("dbo.UserProfiles");
            DropTable("dbo.Posts");
            DropTable("dbo.Topics");
            DropTable("dbo.Categories");
            DropTable("dbo.ErrorLogs");
        }
    }
}
