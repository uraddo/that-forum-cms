namespace ThatForumCMS.DAL.Migrations.ThatForumCMSContextMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserProfileRefactoring : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.UserProfileAbouts", "About", c => c.String(maxLength: 800));
            AlterColumn("dbo.UserProfileSignatures", "Signature", c => c.String(maxLength: 300));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserProfileSignatures", "Signature", c => c.String(nullable: false, maxLength: 300));
            AlterColumn("dbo.UserProfileAbouts", "About", c => c.String(nullable: false, maxLength: 800));
        }
    }
}
