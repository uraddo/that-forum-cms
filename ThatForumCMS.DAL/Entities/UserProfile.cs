﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThatForumCMS.DAL.Entities.Identity {
    //public enum BanStatusEnum {
    //    TemporaryBanned,
    //    PeranentlyBanned
    //}

    //public class BanStatus {
    //    public int Id { get; set; }
    //    public BanStatusEnum UserStatus { get; set; }
    //    public int? UserProfileId { get; set; }
    //    public virtual UserProfile UserProfile { get; set; }
    //    public DateTime BanStart { get; set; }
    //    public DateTime? BanEnd { get; set; }
    //}
    public class UserProfileSignature {
        [ForeignKey("UserProfile")]
        public int UserProfileSignatureId { get; set; }
        /// <summary>
        /// Users can have signature in every post
        /// </summary>
        [MaxLength(300)]
        public string Signature { get; set; }
        public virtual UserProfile UserProfile { get; set; }
    }
    public class UserProfileAbout {
        [ForeignKey("UserProfile")]
        public int UserProfileAboutId { get; set; }
        /// <summary>
        /// Visible on user Profile page
        /// </summary>
        [MaxLength(800)]
        public string About { get; set; }
        public virtual UserProfile UserProfile { get; set; }
    }

    public class UserProfile {

        public int Id { get; set; }
        [Required]
        public string ApplicationUserId { get; set; }
        public string AvatarUrl { get; set; }
        public string RealName { get; set; }
        //public string Login { get; set; }
        
        [Required]
        public DateTime AccountCreationDate { get; set; }

        [Required]
        public DateTime LastVisit { get; set; }
        public bool ProfileHidden { get; set; }
        public virtual UserProfileSignature UserProfileSignature { get; set; }
        public virtual UserProfileAbout UserProfileAbout { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
        public virtual ICollection<Topic> Topics { get; set; }
        public UserProfile() {
            Posts = new HashSet<Post>();
            Topics = new HashSet<Topic>();
        }

        // LATER:
        // friendlist
        //public DateTime BirthDate{get;set;}
        //public virtual ICollection<BanStatus> BanStatuses { get; set; }
        //public UserProfile() {
        //    BanStatuses = new HashSet<BanStatus>();
        //}

        //public virtual ApplicationUser ApplicationUser { get; set; }

        
    }
}
