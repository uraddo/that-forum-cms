﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThatForumCMS.DAL.Entities {

    public class Tag {

        public int Id { get; set; }

        [Required]
        public string Name{ get; set; }

        public virtual ICollection<Topic> Topics { get; set; }

        //public int State { get; set; }// TODO tag states - deleted,...// is it required???
        public Tag() {
            Topics = new HashSet<Topic>();
        }
        
    }
    
}
