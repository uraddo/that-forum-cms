﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThatForumCMS.DAL.Entities {
    public class ErrorLog {

        public int Id { get; set; }

        // 1 - lowest, 10 - highest
        [Range(1, 10)]
        [Required]
        public byte Priority { get; set; }

        [Required]
        public string CmsMessage { get; set; }

        public string ExceptionMsg { get; set; }
        public string ExceptionType { get; set; }
        public string ExceptionSource { get; set; }
        public string ExceptionURL { get; set; }

        [Required]
        public DateTime Logdate { get; set; }

    }
}
