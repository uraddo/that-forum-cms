﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.DAL.Entities.Identity;

namespace ThatForumCMS.DAL.Entities {
    public class Post {

        public int Id { get; set; }
        
        public int UserProfileId { get; set; }
        [ForeignKey("UserProfileId")]
        public virtual UserProfile UserProfile { get; set; }

        [Required]
        public string Message { get; set; }

        [Required]
        public DateTime PostTime { get; set; }

        public PostStatus PostStatus { get; set; }

        public int TopicId { get; set; }
        public virtual Topic Topic { get; set; }

    }

    public enum PostStatus {
        None = 0,
        Normal,
        AwaitingModeration,
        Deleted
    }
}
