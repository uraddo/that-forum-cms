﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.DAL.Entities.Identity;

namespace ThatForumCMS.DAL.Entities {
    public enum TopicState {
        Open,
        UnderModeration,
        Closed,
        Deleted
    }
    public class Topic {
        
        public int Id { get; set; }

        [Required]
        public int CategoryId { get; set; }
        //[Required]
        public virtual Category Category { get; set; }

        public int? UserProfileId { get; set; }
        [ForeignKey("UserProfileId")]
        public virtual UserProfile UserProfile { get; set; }

        [Required]
        public string Title { get; set; }
        public int ViewsCount { get; set; }
        public bool IsPinned { get; set; }
        public TopicState TopicState { get; set; }
        public DateTime CreationDate { get; set; }

        //public int PrimaryPostId { get; set; }
        //[Required]
        //[ForeignKey("PrimaryPostId")]
        //public virtual Post Post { get; set; }


        public ICollection<Post> Posts { get; set; }

        public ICollection<Tag> Tags { get; set; }

        public Topic() {
            Tags = new HashSet<Tag>();
            Posts = new HashSet<Post>();
        }
    }
}
