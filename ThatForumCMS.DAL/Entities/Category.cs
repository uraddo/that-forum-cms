﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThatForumCMS.DAL.Entities {
    public class Category {

        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public int? ParentCategoryId { get; set; }
        public virtual Category ParentCategory { get; set; }
        
        public virtual ICollection<Topic> Topics { get; set; }

        public Category() {
            Topics = new HashSet<Topic>();
        }
    }
}
