﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThatForumCMS.DAL.Entities {
    public class PostLike {

        public int Id { get; set; }

        public virtual Post Post { get; set; }
        public int PostId { get; set; }

        public int ForUser { get; set; }// TODO how doest it will work with identity
        public int ByUser { get; set; }// TODO how doest it will work with identity

    }
}
