﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.DAL.Entities.Identity;

namespace ThatForumCMS.DAL.Identity {
    public class ApplicationRoleManager : RoleManager<ApplicationRole> {
        public ApplicationRoleManager(RoleStore<ApplicationRole> store)
                    : base(store) {


        }
    }
}
