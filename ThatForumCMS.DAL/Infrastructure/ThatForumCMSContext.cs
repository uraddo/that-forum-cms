﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.DAL.Entities;
using ThatForumCMS.DAL.Entities.Identity;

namespace ThatForumCMS.DAL.Infrastructure {

    public class ThatForumCMSContext : DbContext {
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Topic> Forums { get; set; }
        public DbSet<Category> ForumCategories { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<ErrorLog> ErrorLogs { get; set; }


        public ThatForumCMSContext(string connectionString) :base(connectionString) {
            Database.SetInitializer<ThatForumCMSContext>(new ContextInit());
        }
    }
}
