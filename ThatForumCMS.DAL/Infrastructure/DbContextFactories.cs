﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThatForumCMS.DAL.Infrastructure {
    class MigrationsIdentityContextFactory : IDbContextFactory<IdentityContext> {

        public IdentityContext Create() {
            return new IdentityContext("DefaultConnection");
        }
    }

    class MigrationsThatForumCMSContextFactory : IDbContextFactory<ThatForumCMSContext> {

        public ThatForumCMSContext Create() {
            return new ThatForumCMSContext("DefaultConnection");
        }
    }
}
