﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.DAL.Entities.Identity;
using ThatForumCMS.DAL.Identity;
using ThatForumCMS.DAL.Infrastructure;

namespace ThatForumCMS.DAL.Infrastructure {
    public class IdentityContext : IdentityDbContext<ApplicationUser> {
        public IdentityContext(string conectionString) : base(conectionString) { }

        static IdentityContext() {
            Database.SetInitializer<IdentityContext>(new IdentityInitializer());
        }
    }

    internal class IdentityInitializer : CreateDatabaseIfNotExists<IdentityContext> {
        protected override void Seed(IdentityContext context) {
            base.Seed(context);

            foreach (ForumUserRoles roleEnum in Enum.GetValues(typeof(ForumUserRoles))) {
                var role = new ApplicationRole(roleEnum.ToString());
                context.Roles.Add(role);
            }
            context.SaveChanges();
            
        }
    }
}
