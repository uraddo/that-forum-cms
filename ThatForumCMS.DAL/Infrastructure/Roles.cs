﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThatForumCMS.DAL.Infrastructure {
    public enum ForumUserRoles {
        Guest = 0,
        User = 1,
        Moderator = 2,
        TrustedModerator = 3,
        Admin = 4,
        SuperAdmin = 5
    }

    //public static class RolesEnumExtentions {
    //    public static string ToRoleString(this ForumUserRoles roles) {
    //        // TODO LOC
    //        switch (roles) {
    //            case ForumUserRoles.None:
    //                return "norole";
    //            case ForumUserRoles.SuperAdmin:
    //                return "superadmin";
    //            case ForumUserRoles.Admin:
    //                return "admin";
    //            case ForumUserRoles.Moderator:
    //                return "moderator";
    //            case ForumUserRoles.User:
    //                return "user";
    //            case ForumUserRoles.Guest:
    //                return "guest";
    //            default:
    //                return "There is no such user role";
    //        }
    //    }
    //    public static string ToFriendlyString(this ForumUserRoles roles) {
    //        // TODO LOC
    //        switch (roles) {
    //            case ForumUserRoles.None:
    //                return "No Role";
    //            case ForumUserRoles.SuperAdmin:
    //                return "Super Admin";
    //            case ForumUserRoles.Admin:
    //                return "Administaror";
    //            case ForumUserRoles.Moderator:
    //                return "Moderator";
    //            case ForumUserRoles.User:
    //                return "User";
    //            case ForumUserRoles.Guest:
    //                return "Guest";
    //            default:
    //                return "There is no such user role";
    //        }
    //    }
    //}

}
