﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.DAL.Entities;
using ThatForumCMS.DAL.Entities.Identity;
using ThatForumCMS.DAL.Infrastructure;
using ThatForumCMS.DAL.Interfaces;
using ThatForumCMS.DAL.Repositories.Identity;

namespace ThatForumCMS.DAL.Repositories {
    public class ForumUnitOfWork : IForumUnitOfWork, IDisposable {

        private ThatForumCMSContext db;

        #region Inner Repositories
        private IGenericRepository<UserProfile> userProfileRepository;
        private IGenericRepository<Topic> forumRepository;
        private IGenericRepository<Category> forumCategoryRepository;
        private IGenericRepository<Post> postRepository;
        private IGenericRepository<Tag> tagRepository;
        private IGenericRepository<ErrorLog> errorLogRepository;
        private IGenericRepository<UserProfileAbout> userProfileAboutRepository;
        private IGenericRepository<UserProfileSignature> userProfileSignatureRepository;
        #endregion

        public ForumUnitOfWork(string connectionString) {
            db = new ThatForumCMSContext(connectionString);
        }

       
        public IGenericRepository<Topic> TopicRepository {
            get {
                if (forumRepository == null) {
                    forumRepository = new GenericRepository<Topic>(db);
                }
                return forumRepository;
            }
        }

        public IGenericRepository<Category> CategoryRepository {
            get {
                if (forumCategoryRepository == null) {
                    forumCategoryRepository = new GenericRepository<Category>(db);
                }
                return forumCategoryRepository;
            }
        }

        public IGenericRepository<Post> PostRepository {
            get {
                if (postRepository == null) {
                    postRepository = new GenericRepository<Post>(db);
                }
                return postRepository;
            }
        }


        public IGenericRepository<Tag> TagRepository {
            get {
                if (tagRepository == null) {
                    tagRepository = new GenericRepository<Tag>(db);
                }
                return tagRepository;
            }
        }

        public IGenericRepository<ErrorLog> ErrorLogRepository {
            get {
                if (errorLogRepository == null) {
                    errorLogRepository = new GenericRepository<ErrorLog>(db);
                }
                return errorLogRepository;
            }
        }

        public IGenericRepository<UserProfile> UserProfileRepository {
            get {
                if (userProfileRepository == null) {
                    userProfileRepository = new GenericRepository<UserProfile>(db);
                }
                return userProfileRepository;
            }
        }

        public IGenericRepository<UserProfileAbout> UserProfileAboutRepository {
            get {
                if (userProfileAboutRepository == null) {
                    userProfileAboutRepository = new GenericRepository<UserProfileAbout>(db);
                }
                return userProfileAboutRepository;
            }
        }

        public IGenericRepository<UserProfileSignature> UserProfileSignatureRepository {
            get {
                if (userProfileSignatureRepository == null) {
                    userProfileSignatureRepository = new GenericRepository<UserProfileSignature>(db);
                }
                return userProfileSignatureRepository;
            }
        }




        #region Dispose
        private bool disposed = false;

        protected virtual void Dispose(bool disposing) {
            if (!this.disposed) {
                if (disposing) {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        public async Task SaveAsync() {
            await db.SaveChangesAsync();
        }

        public void Save() {
            db.SaveChanges();
        }
    }
}
