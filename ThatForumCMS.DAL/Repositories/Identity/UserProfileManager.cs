﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.DAL.Entities.Identity;
using ThatForumCMS.DAL.Infrastructure;
using ThatForumCMS.DAL.Interfaces;

namespace ThatForumCMS.DAL.Repositories.Identity {
    /*
    public class UserProfileManager : IUserProfileManager {

        public ThatForumCMSContext Database { get; set; }

        public UserProfileManager(ThatForumCMSContext db) {
            Database = db ?? throw new ArgumentNullException();
        }

        public void Create(UserProfile item) {
            if (item == null) {
                throw new ArgumentNullException();
            }
            Database.UserProfiles.Add(item);
            Database.SaveChanges();
        }

        public void Delete(UserProfile item) {
            if (item == null) {
                throw new ArgumentNullException();
            }
            Database.UserProfiles.Remove(item);
            Database.SaveChanges();
        }

        public void Dispose() {
            Database.Dispose();
        }

        public UserProfile GetById(string id) {
            if (string.IsNullOrEmpty(id)) {
                throw new ArgumentException("id can't be null or empty");
            }
            return Database.UserProfiles.Where(u => u.Id == id).SingleOrDefault();
        }

        public UserProfile GetByApplicationUserId(string id) {
            if (string.IsNullOrEmpty(id)) {
                throw new ArgumentException("id can't be null or empty");
            }

            return Database.UserProfiles.Where(up => up.ApplicationUserId == id).SingleOrDefault();
        }

        

        //public UserProfile GetByLogin(string login) {
        //    if (string.IsNullOrEmpty(login)) {
        //        throw new ArgumentException("login can't be null or empty");
        //    }
        //    return Database.UserProfiles.Where(u => u.Login == login).SingleOrDefault();
        //}
    }
    */
}
