﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.DAL.Entities.Identity;
using ThatForumCMS.DAL.Identity;
using ThatForumCMS.DAL.Infrastructure;
using ThatForumCMS.DAL.Interfaces;

namespace ThatForumCMS.DAL.Repositories.Identity {
    public class IdentityUnitOfWork : IIdentityUnitOfWork {
        private IdentityContext db;

        private ApplicationUserManager userManager;
        private ApplicationRoleManager roleManager;
        //private IUserProfileManager userProfileManager;

        public IdentityUnitOfWork(string connectionString) {
            db = new IdentityContext(connectionString);
            userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(db));
            roleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(db));
            //userProfileManager = new UserProfileManager(db);
        }

        public ApplicationUserManager UserManager {
            get { return userManager; }
        }

        //public IUserProfileManager UserProfileManager {
        //    get { return userProfileManager; }
        //}

        public ApplicationRoleManager RoleManager {
            get { return roleManager; }
        }

        public async Task SaveAsync() {
            await db.SaveChangesAsync();
        }

        #region Dispose Pattern

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing) {
            if (!this.disposed) {
                if (disposing) {
                    userManager.Dispose();
                    roleManager.Dispose();
                    //userProfileManager.Dispose();
                }
                this.disposed = true;
            }
        }
        #endregion
    }
}