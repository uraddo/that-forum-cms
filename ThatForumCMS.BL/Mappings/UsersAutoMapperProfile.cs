﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ThatForumCMS.DAL.Entities.Identity;
using ThatForumCMS.BL.EntitiesDTO;

namespace ThatForumCMS.BL.Mappings {
    public class UsersAutoMapperProfile : Profile {
        public UsersAutoMapperProfile() {

            //CreateMap<UserDTO, ApplicationUser>();
            CreateMap<ApplicationUser, UserDTO>()
                //.ForMember(dest => dest.RealName, o => o.MapFrom(src => src.UserProfile.RealName))
                .ForMember(dest => dest.Login, o => o.MapFrom(src => src.UserName));

            CreateMap<UserProfile, UserDTO>();
            CreateMap<UserProfile, UserProfileDTO>()
                .ForMember(dest => dest.About, o => o.MapFrom(src => src.UserProfileAbout.About))
                .ForMember(dest => dest.Signature, o => o.MapFrom(src => src.UserProfileSignature.Signature));

        }
    }

}
