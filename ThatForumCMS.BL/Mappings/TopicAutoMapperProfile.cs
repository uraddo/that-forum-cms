﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.BL.EntitiesDTO;
using ThatForumCMS.DAL.Entities;

namespace ThatForumCMS.BL.Mappings {
    public class TopicAutoMapperProfile : Profile {
        public TopicAutoMapperProfile() {
            CreateMap<TopicDTO, Topic>();
            CreateMap<Topic, TopicDTO>()
                .ForMember(dest => dest.CategoryName, o => o.MapFrom(src => src.Category.Name));
        }
    }
}
