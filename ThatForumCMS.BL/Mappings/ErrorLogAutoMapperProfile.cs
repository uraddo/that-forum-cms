﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ThatForumCMS.BL.EntitiesDTO;
using ThatForumCMS.DAL.Entities;

namespace ThatForumCMS.BL.Mappings {
    public class ErrorLogAutoMapperProfile : Profile {
        public ErrorLogAutoMapperProfile() {
            CreateMap<ErrorLog, ErrorLogDTO>();
        }
    }
}
