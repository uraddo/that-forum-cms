﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ThatForumCMS.DAL.Entities;
using ThatForumCMS.BL.EntitiesDTO;

namespace ThatForumCMS.BL.Mappings {
    public class PostAutoMapperProfile : Profile{
        public PostAutoMapperProfile() {
            CreateMap<PostDTO, Post>().ReverseMap();
        }
    }
}
