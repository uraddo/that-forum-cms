﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.BL.EntitiesDTO;
using ThatForumCMS.DAL.Entities;

namespace ThatForumCMS.BL.Mappings {
    public class CategoryAutoMapperProfile : Profile {
        public CategoryAutoMapperProfile() {
            CreateMap<Category, CategoryDTO>().ReverseMap();
            // .ForMember(dest => dest.ParentCategoryName, o => o.MapFrom(src => src.ParentCategory.Name));
            CreateMap<Category, DeleteCategoryDTO>();
                
        }
    }
}
