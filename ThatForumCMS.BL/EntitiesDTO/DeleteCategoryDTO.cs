﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThatForumCMS.BL.EntitiesDTO {
    public class DeleteCategoryDTO {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ChildCategoriesNum { get; set; }
        public bool DeleteAllChildCategories { get; set; }
        /// <summary>
        /// if false, they will be placed directly to the parent category.
        /// if no parent category, there will be error - can't delete posts
        /// </summary>
        public bool DeleteAllCategoryTopics { get; set; }
    }
}
