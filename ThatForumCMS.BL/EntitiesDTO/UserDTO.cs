﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThatForumCMS.BL.EntitiesDTO {
    public class UserDTO {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        /// <summary>
        /// User's Login name
        /// </summary>
        public string Login { get; set; }
        public string RealName { get; set; }
        public ForumUserRolesDTO Role { get; set; }
    }
}
