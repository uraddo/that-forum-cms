﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThatForumCMS.BL.EntitiesDTO {
    public class CategoryDTO {

        public int Id { get; set; }
        public string Name { get; set; }
        public int? ParentCategoryId { get; set; }
        //public string ParentCategoryName { get; set; }// TODO is it required?
    }
}
