﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThatForumCMS.BL.EntitiesDTO {
    public enum ForumUserRolesDTO {
        Guest = 0,
        User = 1,
        Moderator = 2,
        TrustedModerator = 3,
        Admin = 4,
        SuperAdmin = 5
    }
}
