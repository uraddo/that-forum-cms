﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThatForumCMS.BL.EntitiesDTO {
    /// <summary>
    /// Most detailed user info
    /// </summary>
    public class UserProfileDTO {
        public int Id { get; set; }
        public string RealName { get; set; }
        public string AvatarUrl { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public DateTime AccountCreationDate { get; set; }
        public DateTime LastVisit { get; set; }
        public bool ProfileHidden { get; set; }
        public string About { get; set; }
        public string Signature { get; set; }
        public ForumUserRolesDTO Role { get; set; }
    }
}
