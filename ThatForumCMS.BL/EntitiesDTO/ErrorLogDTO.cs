﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThatForumCMS.BL.EntitiesDTO {
    public class ErrorLogDTO {
        public int Id { get; set; }
        /// <summary>
        ///  1 - lowest, 10 - highest
        /// </summary>
        public byte Priority { get; set; }
        public string CmsMessage { get; set; }
        public string ExceptionMsg { get; set; }
        public string ExceptionType { get; set; }
        public string ExceptionSource { get; set; }
        public string ExceptionURL { get; set; }
        public DateTime Logdate { get; set; }
    }
}
