﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThatForumCMS.BL.EntitiesDTO {
    public class PostDTO {

        public int Id { get; set; }
        public int UserProfileId { get; set; }
        public string Message { get; set; }
        public DateTime PostTime { get; set; }
        public PostStatusDTO PostStatus { get; set; }
        public int TopicId { get; set; }
    }

    public enum PostStatusDTO {
        None = 0,
        Normal,
        AwaitingModeration,
        Deleted
    }
}
