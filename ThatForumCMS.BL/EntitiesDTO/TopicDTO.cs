﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThatForumCMS.BL.EntitiesDTO {
    public class TopicDTO {
        
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }// TODO do i need this?
        public string Title { get; set; }
        public string Author { get; set; }
        public int ViewsCount { get; set; }
        public bool IsPinned { get; set; }
        public int State { get; set; }
        public DateTime CreationDate { get; set; }
    }

    public class UpdateTopicDTO {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Title { get; set; }
        public bool IsPinned { get; set; }
        public int State { get; set; }
        public string Message { get; set; }
    }
}
