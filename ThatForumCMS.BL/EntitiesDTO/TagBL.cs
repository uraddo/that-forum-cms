﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThatForumCMS.BL.EntitiesDTO {
    public class TagBL {

        public int Id { get; set; }
        public string Name{ get; set; }
        public int ForumId { get; set; }
    }
}
