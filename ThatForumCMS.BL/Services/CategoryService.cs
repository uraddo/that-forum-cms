﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.BL.EntitiesDTO;
using ThatForumCMS.BL.Interfaces;
using ThatForumCMS.DAL.Interfaces;
using AutoMapper;
using ThatForumCMS.DAL.Entities;
using System.Linq.Expressions;

namespace ThatForumCMS.BL.Services {
    public class CategoryService : ICategoryService{
        private IForumUnitOfWork Db { get; set; }

        public CategoryService(IForumUnitOfWork unitOfWork) {
            this.Db = unitOfWork;

        }
        public void Create(CategoryDTO category) {
            if (category == null) {
                throw new ArgumentNullException();
            }

            var categ = Mapper.Map<CategoryDTO, Category>(category);
            
            Db.CategoryRepository.Insert(categ);
            Db.Save();
        }

        //public void Delete(int id) {
        //    Db.CategoryRepository.Delete(id);
        //    Db.Save();
        //}

        public void Dispose() {
            Db.Dispose();
        }


        public void Update(CategoryDTO category) {
            if (category == null) {
                throw new ArgumentNullException("category can't be null");
            }
            var categ = Mapper.Map<CategoryDTO, Category>(category);

            Db.CategoryRepository.Update(categ);
            Db.Save();
        }


        public void Delete(DeleteCategoryDTO category) {
            if (category == null) {
                throw new ArgumentNullException("category", "Category can't be null");
            }

            var childNum = GetCategoryChildNumber(category.Id);

            // cascase delete
            if (childNum > 0) {
                if (!category.DeleteAllChildCategories) {
                    throw new Exception("Can't delete category since child categories exists");
                } else {

                    var childCategs = Db.CategoryRepository.Get(filter: BuildParentCategoryIdEqualsExpression(category.Id));
                    foreach (var item in childCategs) {
                        Db.CategoryRepository.Delete(item);
                    }
                }
            }

            Db.CategoryRepository.Delete(category.Id);
            Db.Save();
        }


        #region Get Methods
        public IEnumerable<CategoryDTO> GetAllCategories() {

            var categories = Db.CategoryRepository.Get();
            if (categories == null) {
                return null;
            }
            return Mapper.Map<IEnumerable<Category>, IEnumerable<CategoryDTO>>(categories);
        }

        public IEnumerable<CategoryDTO> GetAllParentCategories() {
            
            var categories = Db.CategoryRepository.Get(filter: BuildParentCategoryIdEqualsExpression(null));
            if (categories == null) {
                return null;
            }
            return Mapper.Map<IEnumerable<Category>, IEnumerable<CategoryDTO>>(categories);
        }

        public CategoryDTO GetCategoryById(int id) {
            var category = Db.CategoryRepository.GetByID(id);
            return Mapper.Map<Category, CategoryDTO>(category);
        }


        public DeleteCategoryDTO GetCategoryInfoForDeletion(int id) {
            var category = Db.CategoryRepository.GetByID(id);
            var deleteCategoryDTO = Mapper.Map<Category, DeleteCategoryDTO>(category);

            // if this category is parent category, calculate number of child categories
            if (category.ParentCategoryId == null) {
                
                deleteCategoryDTO.ChildCategoriesNum = Db.CategoryRepository.Get(
                    filter: BuildParentCategoryIdEqualsExpression(deleteCategoryDTO.Id)).Count();
            }

            return deleteCategoryDTO;
        }

        
        

        public int GetCategoryChildNumber(int id) {
            
            return Db.CategoryRepository.Get(filter: BuildParentCategoryIdEqualsExpression(id)).Count();
        }

        public IEnumerable<CategoryDTO> GetChildCategories(int parentCategoryId) {
            var childCategories = Db.CategoryRepository.Get(Util.BLUtil<Category>
                .BuildEqualsExpression("ParentCategoryId", typeof(int?), (int?)parentCategoryId));

            return Mapper.Map<IEnumerable<Category>, List<CategoryDTO>>(childCategories); ;
        }
        #endregion

        #region Util
        private Expression<Func<Category, bool>> BuildParentCategoryIdEqualsExpression(int? id) {
            ParameterExpression param = Expression.Parameter(typeof(Category));
            Expression boby = Expression.Equal(Expression.PropertyOrField(param, "ParentCategoryId"),
                  Expression.Constant(id, typeof(int?)));
            Expression<Func<Category, bool>> filter = Expression.Lambda<Func<Category, bool>>(boby, param);
            return filter;
        }
        #endregion
    }
}
