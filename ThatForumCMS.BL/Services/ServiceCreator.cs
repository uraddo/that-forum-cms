﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.BL.Interfaces;
using ThatForumCMS.DAL.Repositories;
using ThatForumCMS.DAL.Repositories.Identity;

namespace ThatForumCMS.BL.Services {
    public class ServiceCreator : IServiceCreator {
        public IUserService CreateUserService(string connection) {
            return new UserService(new IdentityUnitOfWork(connection), new ForumUnitOfWork(connection));
        }

        //public IErrorLoggerService CreateErrorLoggerService(string connection) {
        //    return new ErrorLoggerService(new ForumUnitOfWork(connection));
        //}

        //public ITopicService CreateTopicService(string connection) {
        //    return new TopicService(new UnitOfWork(connection));
        //}
    }
}
