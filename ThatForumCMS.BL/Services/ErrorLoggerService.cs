﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.BL.EntitiesDTO;
using ThatForumCMS.BL.Interfaces;
using ThatForumCMS.DAL.Entities;
using ThatForumCMS.DAL.Interfaces;
using AutoMapper;

namespace ThatForumCMS.BL.Services {
    public class ErrorLoggerService : IErrorLoggerService {

        IForumUnitOfWork DB { get; set; }

        public ErrorLoggerService(IForumUnitOfWork uow) {
            DB = uow;
        }

        public void Dispose() {
            DB.Dispose();
        }

        
        /// <param name="priority">1 - lowest, 10 - highest</param>
        public void Log(string message, byte priority = 1, string url = null, Exception exception = null) {

            if (string.IsNullOrEmpty(message)) {
                throw new ArgumentException("message can't be null or empty");
            }

            ErrorLog log = new ErrorLog()
            {
                CmsMessage = message
            };
            if (exception != null) {
                while (exception.InnerException != null) exception = exception.InnerException;
                log.ExceptionMsg = exception.Message;
                log.ExceptionSource = exception.Source;
                log.ExceptionType = exception.GetType().ToString();
            }
            if (!string.IsNullOrEmpty(url)) {
                log.ExceptionURL = url;
            }

            log.Priority = priority;
            log.Logdate = DateTime.Now;

            DB.ErrorLogRepository.Insert(log);
            DB.Save();
        }

        public IEnumerable<ErrorLogDTO> GetErrorLogs(int takeNum, int skip) {

            //var errors = DB.ErrorLogRepository.Get(orderBy: e=>e.OrderByDescending(log => log.Logdate)).Skip(skip).Take(takeNum);
            var errors = DB.ErrorLogRepository.Get(orderBy: e => e.OrderByDescending(log => log.Logdate), skip: skip, take: takeNum);

            var errorLogDTOs = Mapper.Map<IEnumerable<ErrorLog>, List<ErrorLogDTO>>(errors);

            return errorLogDTOs;
        }

        public int TotalErrorLogsNumber() {
            return DB.ErrorLogRepository.Get().Count();
        }
    }
}
