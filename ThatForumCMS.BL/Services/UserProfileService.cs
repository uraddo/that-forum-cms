﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.BL.Infrastructure;
using ThatForumCMS.BL.Interfaces;
using ThatForumCMS.DAL.Interfaces;

namespace ThatForumCMS.BL.Services {
    public class UserProfileService : IUserProfileService {
        private IForumUnitOfWork Db { get; set; }
        
        public UserProfileService(IForumUnitOfWork unitOfWork) {
            this.Db = unitOfWork;

        }
        public void SetAvatar(int userProfileId, string url) {
            // don't check string.isnullorempty to url since user may want just to delete any avatar

            var userProfile = Db.UserProfileRepository.GetByID(userProfileId);
            if (userProfile == null) {
                throw new ArgumentException("No such user exists", "userProfileId");
            }

            userProfile.AvatarUrl = url;
            Db.UserProfileRepository.Update(userProfile);
            Db.Save();
        }

        public void SetUserProfileSignature(int userProfileId, string signatureTxt) {
            var userProfile = Db.UserProfileRepository.GetByID(userProfileId);
            if (userProfile == null) {
                throw new ArgumentException("No such user exists", "userProfileId");
            }

            var userProfileSignature = Db.UserProfileSignatureRepository.GetByID(userProfileId);

            if (userProfileSignature == null) {
                userProfileSignature = new DAL.Entities.Identity.UserProfileSignature()
                {
                    UserProfileSignatureId = userProfileId,
                    Signature = signatureTxt
                };
                Db.UserProfileSignatureRepository.Insert(userProfileSignature);
            } else {
                userProfileSignature.Signature = signatureTxt;
                Db.UserProfileSignatureRepository.Update(userProfileSignature);
            }

            
            Db.Save();
        }

        public void SetUserProfileAbout(int userProfileId, string aboutTxt) {
            var userProfile = Db.UserProfileRepository.GetByID(userProfileId);
            if (userProfile == null) {
                throw new ArgumentException("No such user exists", "userProfileId");
            }

            var userProfileAbout = Db.UserProfileAboutRepository.GetByID(userProfileId);
            if(userProfileAbout == null) {
                userProfileAbout = new DAL.Entities.Identity.UserProfileAbout()
                {
                    UserProfileAboutId = userProfileId,
                    About = aboutTxt
                };
                Db.UserProfileAboutRepository.Insert(userProfileAbout);
            } else {
                userProfileAbout.About = aboutTxt;
                Db.UserProfileAboutRepository.Update(userProfileAbout);
            }
            

            
            Db.Save();
        }

        public string GetAvatarUrl(int userProfileId) {
            var userProfile = Db.UserProfileRepository.GetByID(userProfileId);
            if (userProfile == null) {
                throw new ArgumentException("No such user exists", "userProfileId");
            }

            return userProfile.AvatarUrl;
        }

        public string GetUserProfileSignature(int userProfileId) {
            var userSign = Db.UserProfileSignatureRepository.GetByID(userProfileId);
            if (userSign == null) {
                //throw new ArgumentException("No such user exists", "userProfileId");
                return null;
            }

            return userSign.Signature;
        }

        public string GetUserProfileAbout(int userProfileId) {
            var usersAbout = Db.UserProfileAboutRepository.GetByID(userProfileId);
            if (usersAbout == null) {
                //throw new ArgumentException("No such user exists", "userProfileId");
                return null;
            }

            return usersAbout.About;
        }
    }
}
