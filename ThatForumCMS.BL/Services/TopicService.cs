﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.BL.EntitiesDTO;
using ThatForumCMS.BL.Interfaces;
using ThatForumCMS.BL.Util;
using ThatForumCMS.DAL.Entities;
using ThatForumCMS.DAL.Interfaces;

namespace ThatForumCMS.BL.Services {
    public class TopicService : ITopicService {

        private IForumUnitOfWork Db { get; set; }

        public TopicService(IForumUnitOfWork unitOfWork) {
            this.Db = unitOfWork;
            
        }

        // TODO rewrite this code to use single dto
        public int AddTopic(TopicDTO topicDTO, PostDTO primaryPost, int userProfileId) {

            if (topicDTO == null ) {
                throw new ArgumentException("Argument can't be null", "topicDTO");
            }
            if (primaryPost == null) {
                throw new ArgumentException("Argument can't be null", "primaryPost");
            }
            var postAuthor = Db.UserProfileRepository.GetByID(userProfileId);
            if (postAuthor == null) {
                throw new ArgumentException("No such user exists", "userProfileId");
            }

            var topic = Mapper.Map<TopicDTO, Topic>(topicDTO);
            var post = Mapper.Map<PostDTO, Post>(primaryPost);

            topic.UserProfileId = userProfileId;
            var category = Db.CategoryRepository.GetByID(topicDTO.CategoryId);

            topic.Category = category ?? throw new Exception("No such category exists");
            topic.CreationDate = DateTime.Now;

            post.Topic = topic;
            post.PostTime = DateTime.Now;
            Db.PostRepository.Insert(post);
            Db.TopicRepository.Insert(topic);

            Db.Save();
            
            // Get added topic id and return it
            var topicId = postAuthor.Topics.OrderByDescending(t => t.CreationDate).First().Id;
            return topicId;

        }

        public void Dispose() {
            Db.Dispose();
        }

        public TopicDTO GetTopic(int id) {
            var topic = Db.TopicRepository.GetByID(id);

            var topicDTO = Mapper.Map<Topic, TopicDTO>(topic);

            return topicDTO;
        }

        public IEnumerable<TopicDTO> GetAllTopics() {
            var allTopics = Db.TopicRepository.Get();
            
            return Mapper.Map<IEnumerable<Topic>, List<TopicDTO>>(allTopics);

        }

        public IEnumerable<TopicDTO> GetTopicsFromCategoryOrderByDate(int categoryId, int take) {

            var filter = Util.BLUtil<Topic>.BuildEqualsExpression("CategoryId", typeof(int), categoryId);

            var allTopicsFromCategory = Db.TopicRepository.Get(filter: filter, orderBy: t => t.OrderByDescending(t2 => t2.CreationDate), take: take).ToList();

            var allTopicDTOs = Mapper.Map<IEnumerable<Topic>, List<TopicDTO>>(allTopicsFromCategory);

            for (int i = 0; i < allTopicDTOs.Count; i++) {
                // todo change this: i store appuserId in author and then in controller i'll get username from it
                allTopicDTOs[i].Author = Db.UserProfileRepository.GetByID(allTopicsFromCategory[i].UserProfileId).ApplicationUserId;
            }

            return allTopicDTOs;
        }

        public IEnumerable<TopicDTO> GetTopicsFromCategory(int categoryId) {

            var filter = Util.BLUtil<Topic>.BuildEqualsExpression("CategoryId", typeof(int), categoryId);
            
            var allTopicsFromCategory = Db.TopicRepository.Get(filter: filter).ToList();
            var allTopicDTOs = Mapper.Map<IEnumerable<Topic>, List<TopicDTO>>(allTopicsFromCategory);

            for (int i = 0; i < allTopicDTOs.Count; i++) {
                // todo change this: i store appuserId in author and then in controller i'll get username from it
                allTopicDTOs[i].Author = Db.UserProfileRepository.GetByID(allTopicsFromCategory[i].UserProfileId).ApplicationUserId;
            }

            return allTopicDTOs;
        }

        public void AddViewToTopic(int topicId) {
            var topic = Db.TopicRepository.GetByID(topicId);
            if (topic == null) {
                throw new ArgumentException("There is no such topic with this Id", "topicId");
            }

            topic.ViewsCount += 1;
            Db.TopicRepository.Update(topic);
            Db.Save();
        }

        public IEnumerable<TopicDTO> GetLastTopics(int take, int skip) {
            //var allTopicsFromCategory = Db.TopicRepository.Get().OrderByDescending(c => c.CreationDate).Skip(skip).Take(take).ToList();
            var allTopicsFromCategory = Db.TopicRepository.Get(orderBy: t => t.OrderByDescending(t2=>t2.CreationDate), skip: skip,take: take).ToList();

            var allTopicDTOs = Mapper.Map<IEnumerable<Topic>, List<TopicDTO>>(allTopicsFromCategory);

            for (int i = 0; i < allTopicDTOs.Count; i++) {
                // todo change this: i store appuserId in author and then in controller i'll get username from it
                allTopicDTOs[i].Author = Db.UserProfileRepository.GetByID(allTopicsFromCategory[i].UserProfileId).ApplicationUserId;
            }

            return allTopicDTOs;
        }

        public int GetAllTopicsCount() {
            return Db.TopicRepository.Get().Count();
        }

        public void Update(UpdateTopicDTO topic) {

            var filter = Util.BLUtil<Topic>.BuildEqualsExpression("Id", typeof(int), topic.Id);
            var topicInDb = Db.TopicRepository.Get(filter, includeProperties: "Posts").SingleOrDefault();

            if (topicInDb == null) {
                throw new ArgumentException("No such topic exists");
            }
            var post = topicInDb.Posts.Where(p => p.TopicId == topic.Id).OrderBy(p => p.PostTime).FirstOrDefault();
            if (topicInDb == null) {
                throw new ArgumentException("Topic has no primary post");
            }
            post.Message = topic.Message;
            Db.PostRepository.Update(post);

            topicInDb.Title = topic.Title;
            topicInDb.CategoryId = topic.CategoryId;

            Db.TopicRepository.Update(topicInDb);

            Db.Save();

        }

        public void Delete(UpdateTopicDTO topic) {
            if (topic == null) {
                throw new ArgumentNullException("Argument can't be null", "topicDTO");
            }

            var filter = Util.BLUtil<Post>.BuildEqualsExpression("TopicId", typeof(int), topic.Id);

            var thisTopicsPosts = Db.PostRepository.Get(filter);
            if (thisTopicsPosts == null) {
                throw new Exception("Topic doesn't contain posts. It should have at least one - primary post");
            }

            foreach (var post in thisTopicsPosts) {
                Db.PostRepository.Delete(post);
            }

            Db.TopicRepository.Delete(topic.Id);

            Db.Save();
        }

        public IEnumerable<TopicDTO> GetRecentTopicsFromUser(int userProfileId, int? take = null, int? skip = null) {
            var filter = Util.BLUtil<Topic>.BuildEqualsExpression("UserProfileId", typeof(int), userProfileId);
            
            var usersTopics = Db.TopicRepository.Get(filter: filter, orderBy: c=>c.OrderByDescending( t => t.CreationDate), skip: skip, take: take);
            var usersTopicsDTO = Mapper.Map<IEnumerable<Topic>, List<TopicDTO>>(usersTopics);

            return usersTopicsDTO;
        }
    }
}
