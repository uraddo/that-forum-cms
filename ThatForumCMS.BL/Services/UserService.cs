﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.BL.EntitiesDTO;
using ThatForumCMS.BL.Infrastructure;
using ThatForumCMS.BL.Interfaces;
using ThatForumCMS.DAL.Entities.Identity;
using ThatForumCMS.DAL.Interfaces;
using AutoMapper;
using ThatForumCMS.BL.Exceptions;
using System.Linq.Expressions;
using System.Net.Mail;
using ThatForumCMS.DAL.Entities;

namespace ThatForumCMS.BL.Services {
    public class UserService : IUserService {
        IIdentityUnitOfWork DbIdentity { get; set; }
        IForumUnitOfWork DbForum { get; set; }

        public UserService(IIdentityUnitOfWork identityUoW, IForumUnitOfWork forumUoW) {
            DbIdentity = identityUoW;
            DbForum = forumUoW;
        }

        public async Task<OperationDetails> Create(UserDTO userDto) {

            string email = userDto.Email.ToString();
            try {
                var address = new MailAddress(email);
            }
            catch (FormatException) {
                return new OperationDetails(false, "Not correct email address", "Email");
            }

            ApplicationUser user = await DbIdentity.UserManager.FindByEmailAsync(userDto.Email);
            if (user == null) {
                user = new ApplicationUser { Email = userDto.Email, UserName = userDto.Login };

                DbIdentity.UserManager.UserValidator = new UserValidator<ApplicationUser>(DbIdentity.UserManager)
                {
                    AllowOnlyAlphanumericUserNames = true,
                    RequireUniqueEmail = true
                };
                var result = await DbIdentity.UserManager.CreateAsync(user, userDto.Password);
                if (result.Errors.Count() > 0) {
                    return new OperationDetails(false, result.Errors.FirstOrDefault(), "");
                }
                    
                // Add role
                await DbIdentity.UserManager.AddToRoleAsync(user.Id, userDto.Role.ToString());
                await DbIdentity.SaveAsync();

                // Create forum user profile
                UserProfile userProfile = new UserProfile{
                    ApplicationUserId = user.Id,
                    //Email = user.Email,
                    //Login = userDto.Login,
                    RealName = string.IsNullOrEmpty(userDto.RealName) ? "" : userDto.RealName,
                    AccountCreationDate = DateTime.Now,
                    LastVisit = DateTime.Now,                    
                };
                //DbForum.UserProfileManager.Create(userProfile);
                DbForum.UserProfileRepository.Insert(userProfile);
                await DbForum.SaveAsync();
                

                return new OperationDetails(true, "Регистрация успешно пройдена", "");// TODO LOC
            } else {
                return new OperationDetails(false, "Пользователь с таким email уже существует", "Email");// TODO LOC
            }
        }

        public async Task<ClaimsIdentity> Authenticate(UserDTO userDto) {
            ClaimsIdentity claim = null;
            // find the user
            //ApplicationUser user = await Db.UserManager.FindByNameAsync(userDto.Login);// idon't know why i used this
            ApplicationUser user = await DbIdentity.UserManager.FindAsync(userDto.Login, userDto.Password);
            // авторизуем его и возвращаем объект ClaimsIdentity
            if (user != null)
                claim = await DbIdentity.UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            return claim;
        }

        public async Task SetInitialData(UserDTO adminDto, List<string> roles) {
            foreach (string roleName in roles) {
                var role = await DbIdentity.RoleManager.FindByNameAsync(roleName);
                if (role == null) {
                    role = new ApplicationRole { Name = roleName };
                    await DbIdentity.RoleManager.CreateAsync(role);
                }
            }
            await Create(adminDto);
        }

        /// <exception cref="UserHaveNoRoleException">Thrown when user have no role</exception>
        public async Task<IEnumerable<UserDTO>> GetAllUsers() {

            var userDTOs = new List<UserDTO>();
            var allApplicationUsers = DbIdentity.UserManager.Users.ToArray();
            userDTOs = Mapper.Map<IEnumerable<ApplicationUser>, List<UserDTO>>(allApplicationUsers);

            foreach (var user in userDTOs) {
                var role = (await DbIdentity.UserManager.GetRolesAsync(user.Id)).FirstOrDefault();// TODO change back to SingleOrDefault

                if (!string.IsNullOrEmpty(role)) {
                    Enum.TryParse(role, out ForumUserRolesDTO roleDTO);
                    user.Role = roleDTO;
                } else {
                    //throw new UserHaveNoRoleException();
                    user.Role = ForumUserRolesDTO.Guest;
                }
            }

            return userDTOs;
        }
        

        public void Dispose() {
            DbIdentity.Dispose();
        }

        private string GetRoleByUserId(string id) {
            if (string.IsNullOrEmpty(id)) {
                return null;
            }

            var role = DbIdentity.UserManager.GetRoles(id).SingleOrDefault();// TODO now 1 user can have only one role
            if (string.IsNullOrEmpty(role)) {
                //throw new UserHaveNoRoleException();
                return ForumUserRolesDTO.Guest.ToString();
            }
            return role;
        }

        ///// <exception cref="UserHaveNoRoleException">Throws when user have no role</exception>
        //public UserDTO GetUserById(string id) {
        //    if (string.IsNullOrEmpty(id)) {
        //        return null;
        //    }

        //    //var appUser = Db.UserManager.FindById(id.ToString());
        //    var appUser = DbForum.UserProfileManager.GetById(id);
        //    var role = GetRoleByUserId(appUser.Id);

        //    //var userDTO = Mapper.Map<ApplicationUser, UserDTO>(appUser);
        //    var userDTO = Mapper.Map<UserProfile, UserDTO>(appUser);
        //    Enum.TryParse(role, out ForumUserRolesDTO roleDTO);
        //    userDTO.Role = roleDTO;

        //    return userDTO;
        //}


        public async Task<OperationDetails> Update(UserDTO userToUpdate) {

            if (userToUpdate == null) {
                throw new ArgumentNullException("userToUpdate can't be null");
            }
            if (string.IsNullOrEmpty(userToUpdate.Login)) {
                throw new ArgumentException("Login can't be empty", "userToUpdate.Login");
            }
            if (string.IsNullOrEmpty(userToUpdate.Email)) {
                throw new ArgumentException("Email can't be empty", "userToUpdate.Email");
            }

            ApplicationUser appUser = await DbIdentity.UserManager.FindByIdAsync(userToUpdate.Id);

            var hisProfile = GetUserProfileByApplicationUserId(appUser.Id);


            appUser.UserName = userToUpdate.Login;
            appUser.Email = userToUpdate.Email;
            hisProfile.RealName = userToUpdate.RealName;
            DbForum.UserProfileRepository.Update(hisProfile);
            await DbForum.SaveAsync();


            await DbIdentity.UserManager.UpdateAsync(appUser);

            // Update role
            var currentUserRole = (await DbIdentity.UserManager.GetRolesAsync(appUser.Id)).SingleOrDefault();
            var appRole = DbIdentity.RoleManager.FindByName(userToUpdate.Role.ToString());
            if(appRole == null) {
                return new OperationDetails(false, "No such role exists", "");
            }
            
            if (!string.IsNullOrEmpty(currentUserRole) && currentUserRole != userToUpdate.Role.ToString()) {
                DbIdentity.UserManager.RemoveFromRole(appUser.Id, currentUserRole);
            }
            DbIdentity.UserManager.AddToRole(appUser.Id, userToUpdate.Role.ToString());
            await DbIdentity.SaveAsync();

            return new OperationDetails(true, "Succeccfully updated", "");
        }
        private UserProfile GetUserProfileByApplicationUserId(string id) {
            if (string.IsNullOrEmpty(id)) {
                return null;
            }

            ParameterExpression param = Expression.Parameter(typeof(UserProfile));
            Expression boby = Expression.Equal(Expression.PropertyOrField(param, "ApplicationUserId"),
                  Expression.Constant(id, typeof(string)));
            Expression<Func<UserProfile, bool>> filter = Expression.Lambda<Func<UserProfile, bool>>(boby, param);

            
            return DbForum.UserProfileRepository.Get(filter: filter).SingleOrDefault();
        }


        public async Task<OperationDetails> Delete(string id) {

            ApplicationUser appUser = await DbIdentity.UserManager.FindByIdAsync(id);

            var userProfile = GetUserProfileByApplicationUserId(appUser.Id);

            // Find User's topics and posts inside them and delete
            var filter = Util.BLUtil<Topic>.BuildEqualsExpression("UserProfileId", typeof(int?), userProfile.Id);
            var topicsInDb = DbForum.TopicRepository.Get(filter, includeProperties: "Posts").ToList();

            for (int i = 0; i < topicsInDb.Count(); i++) {
                var topic = topicsInDb[i];
                var topicPosts = topic.Posts.ToList();
                for (int j = 0; j < topic.Posts.Count(); j++) {
                    DbForum.PostRepository.Delete(topicPosts[j]);
                }
                DbForum.TopicRepository.Delete(topic);
            }
            //foreach (var item in topicsInDb) {
            //    foreach (var post in item.Posts) {
            //        DbForum.PostRepository.Delete(post);
            //    }
            //    DbForum.TopicRepository.Delete(item);
            //}
            await DbForum.SaveAsync();

            // DELETE posts in other user's topics
            var postFilter = Util.BLUtil<Post>.BuildEqualsExpression("UserProfileId", typeof(int), userProfile.Id);
            var thisUsersPostsInDb = DbForum.PostRepository.Get(postFilter);
            foreach (var item in thisUsersPostsInDb) {
                DbForum.PostRepository.Delete(item);
            }
            await DbForum.SaveAsync();

            // DELETE what left from user
            DbForum.UserProfileRepository.Delete(userProfile.Id);
            await DbForum.SaveAsync();

            IdentityResult result = await DbIdentity.UserManager.DeleteAsync(appUser);
            await DbIdentity.SaveAsync();

            if (result.Errors.Count() > 0) {
                return new OperationDetails(false, result.Errors.FirstOrDefault(), "");
            } else {
                return new OperationDetails(true, "User Successfully deleted", "");
            }

        }

        public UserDTO GetUserByName(string name) {
            if (string.IsNullOrEmpty(name)) {
                return null;
            }

            var appUser = DbIdentity.UserManager.FindByName(name);
            var role = GetRoleByUserId(appUser.Id);
            var userProfile = GetUserProfileByApplicationUserId(appUser.Id);

            var userDTO = Mapper.Map<UserProfile, UserDTO>(userProfile);
            Enum.TryParse(role, out ForumUserRolesDTO roleDTO);
            userDTO.Role = roleDTO;
            
            return userDTO;
        }

        public void OnSignedIn(string login) {
            if (string.IsNullOrEmpty(login)) {
                throw new ArgumentException("id can't be null or empty");
            }
            var user = DbIdentity.UserManager.FindByName(login);
            var userProfile = GetUserProfileByApplicationUserId(user.Id);
            if (userProfile == null) {
                //throw new Nu
            }
            userProfile.LastVisit = DateTime.Now;
            DbForum.UserProfileRepository.Update(userProfile);
            DbForum.Save();

        }

        public UserProfileDTO GetUserProfileByIdentityId(string id) {
            if (string.IsNullOrEmpty(id)) {
                return null;
            }

            var appUser = DbIdentity.UserManager.FindById(id);
            var role = GetRoleByUserId(appUser.Id);
            var userProfile = GetUserProfileByApplicationUserId(appUser.Id);

            var userProfileDTO = Mapper.Map<UserProfile, UserProfileDTO>(userProfile);
            Enum.TryParse(role, out ForumUserRolesDTO roleDTO);
            userProfileDTO.Role = roleDTO;
            userProfileDTO.Login = appUser.UserName;
            userProfileDTO.Email = appUser.Email;

            var about = DbForum.UserProfileAboutRepository.GetByID(userProfileDTO.Id);
            if (about != null) {
                userProfileDTO.About = about.About;
            }

            var signature = DbForum.UserProfileSignatureRepository.GetByID(userProfileDTO.Id);
            if(signature != null) {
                userProfileDTO.Signature = signature.Signature;
            }


            return userProfileDTO;
        }

        public UserProfileDTO GetUserProfileByIdentityName(string name) {
            if (string.IsNullOrEmpty(name)) {
                return null;
            }
            var user = DbIdentity.UserManager.FindByName(name);

            if (user == null) {
                return null;
            }

            return GetUserProfileByIdentityId(user.Id);
        }
        
        public string GetUserNameById(string id) {
            if (string.IsNullOrEmpty(id)) {
                return null;
            }

            var appUser = DbIdentity.UserManager.FindById(id);
            if (appUser == null) {
                return null;
            }

            return appUser.UserName;
        }

        public string GetUserNameByUserProfileId(int userProfileId) {
            var userProfile = DbForum.UserProfileRepository.GetByID(userProfileId);
            if (userProfile == null) {
                //throw new ArgumentException("No such user profile exists", "userProfileId");
                return null;
            }

            var identityUser = DbIdentity.UserManager.FindById(userProfile.ApplicationUserId);
            if (identityUser == null) {
                //throw new Exception("No such ApplicationUser exists");
                return null;
            }

            return identityUser.UserName;
        }

        public int GetUserProfileIdByIdentityId(string id) {
            if (string.IsNullOrEmpty(id)) {
                throw new Exception("No such ApplicationUser exists");
            }

            var userProfile = DbForum.UserProfileRepository
                .Get(Util.BLUtil<UserProfile>.BuildEqualsExpression("ApplicationUserId", typeof(string), id)).SingleOrDefault();

            if (userProfile == null) {
                throw new Exception("No such ApplicationUser exists");
            }

            return userProfile.Id;
        }

        public async Task<OperationDetails> ChangePassword(string identityId, string currentPassword, string newPassword) {
            var user = await DbIdentity.UserManager.FindByIdAsync(identityId);
            if (user == null) {
                return new OperationDetails(false, "Can't find user", "id");
            }
            //if (user.PasswordHash != DbIdentity.UserManager.PasswordHasher.HashPassword(currentPassword)) {
            //    return new OperationDetails(false, "Wrong old password", "currentPassword");
            //}

            var result = await DbIdentity.UserManager.ChangePasswordAsync(user.Id, currentPassword, newPassword);
            if (result.Errors.Count() > 0) {
                return new OperationDetails(false, result.Errors.FirstOrDefault(), "");
            }
            await DbIdentity.SaveAsync();

            return new OperationDetails(true, "Success", "");
        }
    }
}
