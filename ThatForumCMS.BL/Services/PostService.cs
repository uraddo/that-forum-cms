﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.BL.EntitiesDTO;
using ThatForumCMS.BL.Interfaces;
using ThatForumCMS.DAL.Entities;
using ThatForumCMS.DAL.Interfaces;
using AutoMapper;

namespace ThatForumCMS.BL.Services {
    public class PostService : IPostService {
        private IForumUnitOfWork Db { get; set; }

        public PostService(IForumUnitOfWork unitOfWork) {
            this.Db = unitOfWork;
        }

        public void Add(PostDTO postDTO) {

            var postAuthor = Db.UserProfileRepository.GetByID(postDTO.UserProfileId);
            if(postAuthor == null) {
                throw new ArgumentException("No such user exists", "userProfileId");
            }

            var post = Mapper.Map<PostDTO, Post>(postDTO);
            
            Db.PostRepository.Insert(post);
            Db.Save();
        }

        //public IEnumerable<PostDTO> GetAllPosts() {
        //    throw new NotImplementedException();
        //}

        public PostDTO GetPost(int id) {
            try {
                var post = Db.PostRepository.GetByID(id);
                return Mapper.Map<Post, PostDTO>(post);
            }
            catch (InvalidOperationException ex) {
                throw ex;
            }
            
        }

        public void Delete(int id) {
            var post = Db.PostRepository.GetByID(id);
            if (post == null) {
                throw new ArgumentException("No such post exists");
            }

            Db.PostRepository.Delete(post);
            Db.Save();
        }

        public void Update(PostDTO postDTO) {
            if (postDTO == null) {
                throw new ArgumentNullException("post", "post can't be null");
            }
            if (string.IsNullOrEmpty(postDTO.Message)) {
                throw new ArgumentException("postDTO.Message", "Message can't be empty");
            }
            var post = Mapper.Map<PostDTO, Post>(postDTO);
            
            Db.PostRepository.Update(post);
            Db.Save();
        }
        
        public PostDTO GetTopicsPrimaryPost(int topicId) {
            return GetTopicPostsOrderedByDate(topicId, 1, 0).SingleOrDefault();
        }

        public IEnumerable<PostDTO> GetTopicPostsOrderedByDate(int topicId, int takeNum, int skip) {
            var filter = Util.BLUtil<Post>.BuildEqualsExpression("TopicId", typeof(int), topicId);
            var posts = Db.PostRepository.Get(filter: filter, orderBy: p => p.OrderBy(post => post.PostTime), skip: skip, take: takeNum);//.Skip(skip).Take(takeNum)

            var postDTOs = Mapper.Map<IEnumerable<Post>, List<PostDTO>> (posts);

            return postDTOs;
        }

        public int GetTopicsTotalPostsNumber(int topicId) {
            var filter = Util.BLUtil<Post>.BuildEqualsExpression("TopicId", typeof(int), topicId);
            return Db.PostRepository.Get(filter: filter).Count();
        }

        public IEnumerable<PostDTO> GetLastPosts(int takeNum, int skip) {
            //var posts = Db.PostRepository.Get(orderBy: p => p.OrderByDescending(post => post.PostTime)).Skip(skip).Take(takeNum);
            var posts = Db.PostRepository.Get(orderBy: p => p.OrderByDescending(post => post.PostTime), skip: skip, take: takeNum);

            var postDTOs = Mapper.Map<IEnumerable<Post>, List<PostDTO>>(posts);
            //foreach (var post in postDTOs) {
            //    var author = Db.UserProfileRepository.GetByID(post.UserProfileId);
            //    if(author != null) {
            //        post.Author = author.us;
            //    }
            //}
            return postDTOs;
        }

        public int GetTotalPostsNumber() {
            return Db.PostRepository.Get().Count();
        }

        public void UpdateMessage(int postId, string message) {
            var post = Db.PostRepository.GetByID(postId);
            if (post == null) {
                throw new ArgumentException("No such post exists");
            }
            if (string.IsNullOrEmpty(message)) {
                throw new ArgumentException("message", "Message can't be empty");
            }

            post.Message = message;
            Db.PostRepository.Update(post);
            Db.Save();
        }

        //public IEnumerable<PostDTO> GetPosts(Func<PostDTO, bool> filter, int takeNum, Func<IQueryable<PostDTO>, IOrderedQueryable<PostDTO>> orderBy = null) {
        // how do I convert PostsDTO to Post inside a delegate???
        //    Db.PostRepository.Get(orderBy: orderBy);

        //}

        public IEnumerable<PostDTO> GetRecentPostsFromUser(int userProfileId, int? take = null, int? skip = null) {
            var filter = Util.BLUtil<Post>.BuildEqualsExpression("UserProfileId", typeof(int), userProfileId);

            var usersPosts = Db.PostRepository.Get(filter: filter, orderBy: c => c.OrderByDescending(t => t.PostTime), skip: skip, take: take);
            var usersPostsDTO = Mapper.Map<IEnumerable<Post>, List<PostDTO>>(usersPosts);

            return usersPostsDTO;
        }
    }
}
