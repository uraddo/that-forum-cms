﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.BL.Interfaces;
using ThatForumCMS.BL.Services;
using ThatForumCMS.DAL.Interfaces;
using ThatForumCMS.DAL.Repositories;
using ThatForumCMS.DAL.Repositories.Identity;

namespace ThatForumCMS.BL.Infrastructure {
    public class ServiceModule : NinjectModule {

        private string connectionString;

        public ServiceModule(string connection) {
            connectionString = connection;
        }

        public override void Load() {
            Bind<IForumUnitOfWork>().To<ForumUnitOfWork>().WithConstructorArgument(connectionString);
            Bind<IIdentityUnitOfWork>().To<IdentityUnitOfWork>().WithConstructorArgument(connectionString);
        }
    }
}
