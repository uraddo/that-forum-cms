﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.DAL.Entities;

namespace ThatForumCMS.BL.Util {
    public static class BLUtil<T> where T : class {
        public static Expression<Func<T, bool>> BuildEqualsExpression(string paramNameToCheck, Type paramType, object paramValue) {
            
            ParameterExpression param = Expression.Parameter(typeof(T));
            Expression boby = Expression.Equal(Expression.PropertyOrField(param, paramNameToCheck),
                  Expression.Constant(paramValue, paramType));
            Expression<Func<T, bool>> filter = Expression.Lambda<Func<T, bool>>(boby, param);
            return filter;
        }
    }
}
