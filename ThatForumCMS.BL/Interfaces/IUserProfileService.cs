﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.BL.Infrastructure;

namespace ThatForumCMS.BL.Interfaces {
    public interface IUserProfileService {
        string GetAvatarUrl(int userProfileId);
        void SetAvatar(int userProfileId, string url);
        string GetUserProfileSignature(int userProfileId);
        string GetUserProfileAbout(int userProfileId);
        void SetUserProfileSignature(int userProfileId, string signatureTxt);
        void SetUserProfileAbout(int userProfileId, string aboutTxt);
    }
}
