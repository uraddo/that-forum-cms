﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.BL.EntitiesDTO;

namespace ThatForumCMS.BL.Interfaces {
    public interface ICategoryService : IDisposable {
        CategoryDTO GetCategoryById(int id);
        void Create(CategoryDTO category);
        void Update(CategoryDTO category);
        //void Delete(int id);
        void Delete(DeleteCategoryDTO category);
        DeleteCategoryDTO GetCategoryInfoForDeletion(int id);
        IEnumerable<CategoryDTO> GetAllCategories();
        IEnumerable<CategoryDTO> GetAllParentCategories();
        IEnumerable<CategoryDTO> GetChildCategories(int parentCategoryId);
        int GetCategoryChildNumber(int id);
    }
}
