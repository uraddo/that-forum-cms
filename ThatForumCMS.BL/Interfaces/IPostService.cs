﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.BL.EntitiesDTO;
using ThatForumCMS.BL.Infrastructure;

namespace ThatForumCMS.BL.Interfaces {
    public interface IPostService {
        void Add(PostDTO postDTO);//, int userProfileId
        PostDTO GetPost(int id);
        //IEnumerable<PostDTO> GetAllPosts();
        void Delete(int id);
        void Update(PostDTO post);
        /// <summary>
        /// In most cases use this method to update the post
        /// </summary>
        void UpdateMessage(int postId, string message);
        /// <summary>
        /// Get posts sorted by posting date
        /// </summary>
        /// <param name="topicId"></param>
        /// <param name="takeNum">Number of posts to get</param>
        /// <param name="skip">Ignore first [skip]-posts</param>
        IEnumerable<PostDTO> GetTopicPostsOrderedByDate(int topicId, int takeNum, int skip);
        /// <summary>
        /// Get last post sorted by date
        /// </summary>
        /// <param name="takeNum">Number of posts to get</param>
        /// <param name="skip">Ignore first [skip]-posts</param>
        IEnumerable<PostDTO> GetLastPosts(int takeNum, int skip);
        IEnumerable<PostDTO> GetRecentPostsFromUser(int userProfileId, int? take = null, int? skip = null);
        /// <summary>
        /// This number includes topic's primary post
        /// </summary>
        int GetTopicsTotalPostsNumber(int topicId);
        int GetTotalPostsNumber();
        PostDTO GetTopicsPrimaryPost(int topicId);
    }
}
