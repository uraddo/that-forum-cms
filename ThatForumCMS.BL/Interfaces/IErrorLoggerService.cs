﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.BL.EntitiesDTO;

namespace ThatForumCMS.BL.Interfaces {
    public interface IErrorLoggerService : IDisposable {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="priority">1 - lowest, 10 - highest</param>
        /// <param name="url"></param>
        /// <param name="exception"></param>
        void Log(string message, byte priority = 1, string url = null, Exception exception = null);
        IEnumerable<ErrorLogDTO> GetErrorLogs(int takeNum, int skip);
        int TotalErrorLogsNumber();
    }
}
