﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.BL.EntitiesDTO;

namespace ThatForumCMS.BL.Interfaces {
    public interface ITopicService : IDisposable {
        /// <returns>id of newly added topic</returns>
        int AddTopic(TopicDTO topicDTO, PostDTO primaryPost, int userProfileId);
        TopicDTO GetTopic(int id);
        IEnumerable<TopicDTO> GetAllTopics();
        IEnumerable<TopicDTO> GetTopicsFromCategory(int categoryId);
        IEnumerable<TopicDTO> GetTopicsFromCategoryOrderByDate(int categoryId, int take);
        IEnumerable<TopicDTO> GetLastTopics(int take, int skip);
        IEnumerable<TopicDTO> GetRecentTopicsFromUser(int userProfileId, int? take = null, int? skip = null);
        /// <summary>
        /// Adds one view to topic's view count
        /// </summary>
        /// <param name="topicId"></param>
        void AddViewToTopic(int topicId);
        int GetAllTopicsCount();
        void Update(UpdateTopicDTO topic);
        void Delete(UpdateTopicDTO topic);
    }
}
