﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThatForumCMS.BL.Interfaces {
    public interface IServiceCreator {
        IUserService CreateUserService(string connection);
        //IErrorLoggerService CreateErrorLoggerService(string connection);
        //ITopicService CreateTopicService(string connection);
    }
}
