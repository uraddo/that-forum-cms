﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ThatForumCMS.BL.EntitiesDTO;
using ThatForumCMS.BL.Infrastructure;

namespace ThatForumCMS.BL.Interfaces {
    public interface IUserService : IDisposable {
        Task<OperationDetails> Create(UserDTO userDto);
        Task<ClaimsIdentity> Authenticate(UserDTO userDto);
        Task SetInitialData(UserDTO adminDto, List<string> roles);
        Task<IEnumerable<UserDTO>> GetAllUsers();
        UserDTO GetUserByName(string name);
        int GetUserProfileIdByIdentityId(string id);

        /// <summary>
        /// Get UserName aka Login by ApplicationUser Id
        /// </summary>
        string GetUserNameById(string id);
        
        /// <summary>
        /// Get generic info about user
        /// </summary>
        //UserDTO GetUserById(string id);

        /// <summary>
        /// Get Almost all info about user by Identity Name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        UserProfileDTO GetUserProfileByIdentityName(string name);
        
        /// <summary>
        /// Get Almost all info about user
        /// </summary>
        UserProfileDTO GetUserProfileByIdentityId(string id);
        
        /// <summary>
        /// Update user info
        /// </summary>
        Task<OperationDetails> Update(UserDTO userToUpdate);
        
        /// <summary>
        /// Permanently delete user
        /// </summary>
        // TODO will it delete also all user posts???
        Task<OperationDetails> Delete(string id);

        string GetUserNameByUserProfileId(int userProfileId);

        /// <summary>
        /// Change user last visit datetime
        /// </summary>
        /// <param name="id">user's id</param>
        void OnSignedIn(string id);

        Task<OperationDetails> ChangePassword(string identityId, string currentPassword, string newPassword);
    }
}
