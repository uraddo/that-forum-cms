﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThatForumCMS.BL.Exceptions {
    public class UserHaveNoRoleException : Exception {
        public UserHaveNoRoleException() {
        }

        public UserHaveNoRoleException(string message)
        : base(message)
    {
        }

        public UserHaveNoRoleException(string message, Exception inner)
        : base(message, inner)
    {
        }
    }
}
