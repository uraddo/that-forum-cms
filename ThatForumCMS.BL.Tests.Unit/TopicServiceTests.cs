﻿using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit;
using NUnit.Framework;
using ThatForumCMS.BL;
using ThatForumCMS.DAL.Interfaces;
using Moq;
using ThatForumCMS.BL.Services;
using ThatForumCMS.BL.EntitiesDTO;
using ThatForumCMS.DAL.Entities;
using ThatForumCMS.DAL.Infrastructure;
using System.Data.Entity;
using ThatForumCMS.DAL.Repositories;
using ThatForumCMS.DAL.Entities.Identity;
using AutoMapper;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;

namespace ThatForumCMS.BL.Tests.Unit {
    [TestFixture]
    public class TopicServiceTests {

        [OneTimeSetUp]
        public void InitMapper() {
            Mapper.Initialize(config =>
            {
                // BL's modules
                config.AddProfile<ThatForumCMS.BL.Mappings.TopicAutoMapperProfile>();
                config.AddProfile<ThatForumCMS.BL.Mappings.UsersAutoMapperProfile>();
                config.AddProfile<ThatForumCMS.BL.Mappings.CategoryAutoMapperProfile>();
                config.AddProfile<ThatForumCMS.BL.Mappings.PostAutoMapperProfile>();
                config.AddProfile<ThatForumCMS.BL.Mappings.ErrorLogAutoMapperProfile>();

            });
        }

        [Test]
        public void DeleteTopic_NullPasses_ThrowsArgumentNullException() {

            var fakeUnitOfWork = new Mock<IForumUnitOfWork>();
            TopicService topicService = new TopicService(fakeUnitOfWork.Object);


            Assert.Throws<ArgumentNullException>(()=>topicService.Delete(null));
        }

        [Test]
        public void AddTopic_PassValidData_CallsInsertsAndSaves() {

            int userProfileId = 1;
            int categId = 2;
            var topicDTO = new TopicDTO();
            var postDTO = new PostDTO();
            var fakeUnitOfWork = new Mock<IForumUnitOfWork>();
            var userProfile = new UserProfile
            {
                Topics = new List<Topic>{ new Topic() }
            };

            fakeUnitOfWork.Setup(u => u.UserProfileRepository.GetByID(It.IsAny<int>()))
                .Returns(userProfile);
            fakeUnitOfWork.Setup(u => u.CategoryRepository.GetByID(It.IsAny<int>()))
                .Returns(new Category { Id = categId });
            fakeUnitOfWork.Setup(u => u.TopicRepository.Insert(It.IsAny<Topic>()))
                .Callback<Topic>(t => userProfile.Topics.Add(t));
            fakeUnitOfWork.Setup(u => u.PostRepository.Insert(It.IsAny<Post>()));
         

            // Act
            var topicService = new TopicService(fakeUnitOfWork.Object);
            topicService.AddTopic(topicDTO, postDTO, userProfileId);

            // Assert
            fakeUnitOfWork.Verify(u => u.PostRepository.Insert(It.IsAny<Post>()), Times.Once());
            fakeUnitOfWork.Verify(u => u.TopicRepository.Insert(It.IsAny<Topic>()), Times.Once());
            fakeUnitOfWork.Verify(u => u.Save(), Times.Once());
            Assert.IsTrue(userProfile.Topics.Count ==2);
        }

        [Test]
        public void AddTopic_PassInvalidData_Throws() {
            var fakeUnitOfWork = new Mock<IForumUnitOfWork>();
            fakeUnitOfWork.Setup(u => u.UserProfileRepository.GetByID(It.IsAny<int>()))
                .Returns((UserProfile)null);
            fakeUnitOfWork.Setup(u => u.CategoryRepository.GetByID(It.IsAny<int>()))
                .Returns((Category)null);
            // Act
            var topicService = new TopicService(fakeUnitOfWork.Object);
            // Assert
            Assert.Throws<ArgumentException>(() => topicService.AddTopic(null, new PostDTO(), 1));
            Assert.Throws<ArgumentException>(() => topicService.AddTopic(new TopicDTO(), null, 1));
            var ex = Assert.Throws<ArgumentException>(() => topicService.AddTopic(new TopicDTO() {Id =1 }, new PostDTO {Id = 2 }, -91));
            Assert.That(ex.Message, Does.Contain("No such user exists"));
            
        }

        [Test]
        public void AddTopic_NonExistingCategory_Throws() {
            var fakeUnitOfWork = new Mock<IForumUnitOfWork>();
            fakeUnitOfWork.Setup(u => u.CategoryRepository.GetByID(It.IsAny<int>()))
                .Returns((Category)null);
            fakeUnitOfWork.Setup(u => u.UserProfileRepository.GetByID(It.IsAny<int>()))
                .Returns(new UserProfile());
            fakeUnitOfWork.Setup(u => u.TopicRepository.Insert(It.IsAny<Topic>()));
            fakeUnitOfWork.Setup(u => u.PostRepository.Insert(It.IsAny<Post>()));

            // Act
            var topicService = new TopicService(fakeUnitOfWork.Object);
            // Assert
            var exept = Assert.Throws<Exception>(() => topicService.AddTopic(new TopicDTO { Id = 1 }, new PostDTO { Id = 1 }, 1));
            Assert.That(exept.Message, Does.Contain("No such category "));
        }


        [Test]
        public void GetTopic_ExistingTopicId_ReturnsTopicDTO() {
            var topic = new Topic
            {
                Id = 1
            };
            var fakeUnitOfWork = new Mock<IForumUnitOfWork>();
            fakeUnitOfWork.Setup(u => u.TopicRepository.GetByID(It.IsAny<int>()))
                .Returns(topic);

            // Act
            var topicService = new TopicService(fakeUnitOfWork.Object);
            var resultTopicDTO = topicService.GetTopic(3);
            // Assert
            Assert.AreEqual(resultTopicDTO.Id, topic.Id);
        }

        [Test]
        public void GetTopic_NonExistingTopicId_ReturnsNull() {

            var fakeUnitOfWork = new Mock<IForumUnitOfWork>();
            fakeUnitOfWork.Setup(u => u.TopicRepository.GetByID(It.IsAny<int>()))
                .Returns((Topic)null);

            // Act
            var topicService = new TopicService(fakeUnitOfWork.Object);
            var resultTopicDTO = topicService.GetTopic(3);
            // Assert
            Assert.AreEqual(resultTopicDTO, null);
        }

        [Test]
        public void GetAllTopics_Called_InvokesGetFromRepo() {
            var topicsFromDb = new List<Topic>
            {
                new Topic{ Id = 1}, new Topic{Id = 2}
            };
            var fakeUnitOfWork = new Mock<IForumUnitOfWork>();
            fakeUnitOfWork.Setup(u => u.TopicRepository.Get(
                    It.IsAny<Expression<Func<Topic, bool>>>(),
                    It.IsAny<Func<IQueryable<Topic>, IOrderedQueryable<Topic>>>(),
                    It.IsAny<string>(),
                    It.IsAny<int?>(),
                    It.IsAny<int?>()))
                .Returns(topicsFromDb);

            // Act
            var topicService = new TopicService(fakeUnitOfWork.Object);
            var resultTopicDTOs = topicService.GetAllTopics();
            // Assert
            fakeUnitOfWork.Verify(u => u.TopicRepository.Get(null, null, "", null, null), Times.Once());
            Assert.IsTrue(resultTopicDTOs.Count() > 0);
            CollectionAssert.AreEqual(resultTopicDTOs.Select(i => i.Id), topicsFromDb.Select(i => i.Id));
        }


        [Test]
        public void GetTopicsFromCategoryOrderByDate_ReturnsListOfTopics() {
            var topics = new List<Topic>()
            {
                new Topic{ Id = 1, CategoryId = 21}, new Topic{Id = 2, CategoryId = 21},
                new Topic{ Id = 3, CategoryId = 21}, new Topic{Id = 4, CategoryId = 21}
            };
            int requestedCategoryId = 21;
            int take = 1;
            var fakeUnitOfWork = new Mock<IForumUnitOfWork>();
            fakeUnitOfWork.Setup(u => u.UserProfileRepository.GetByID(It.IsAny<object>()))
                .Returns(new UserProfile());
            fakeUnitOfWork.Setup(u => u.TopicRepository.Get(
                   It.Is<Expression<Func<Topic, bool>>>(
                     e => e.ToString() == Util.BLUtil<Topic>.BuildEqualsExpression("CategoryId", typeof(int), requestedCategoryId).ToString()),
                   It.IsAny<Func<IQueryable<Topic>, IOrderedQueryable<Topic>>>(),
                   It.IsAny<string>(),
                   It.IsAny<int?>(),
                   It.Is<int?>(i => i == take)))
               .Returns(topics);
            
            // Act
            var topicService = new TopicService(fakeUnitOfWork.Object);
            var resultTopics = topicService.GetTopicsFromCategoryOrderByDate(requestedCategoryId, take);
            // Assert
            Assert.IsTrue(resultTopics.Count() > 0);
            CollectionAssert.AreEqual(resultTopics.Select(i=>i.Id), topics.Select(i => i.Id));
        }


        //[Test]
        //public void GetTopicsFromCategory_
    }
}
