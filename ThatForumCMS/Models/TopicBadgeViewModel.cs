﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThatForumCMS.Models {
    public class TopicBadgeViewModel {
        public int Id { get; set; }
        public string Title { get; set; }
        public int CommentNumber { get; set; }
        public string Author { get; set; }
        public string LastCommentBy { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? LastCommented { get; set; }
    }
}