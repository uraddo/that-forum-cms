﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThatForumCMS.Models.Users {
    public class PublicUserProfileViewModel {
        public string Id { get; set; }
        public string RealName { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public DateTime AccountCreationDate { get; set; }
        public DateTime LastVisit { get; set; }
        public bool ProfileHidden { get; set; }
        public string About { get; set; }
        public string Signature { get; set; }
        public string AvatarUrl { get; set; }
    }
}