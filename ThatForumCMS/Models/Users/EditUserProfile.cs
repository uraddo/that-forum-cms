﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThatForumCMS.Models.Users {
    public class EditUserProfile {
        public int Id { get; set; }
        public string AvatarUrl { get; set; }
        public string About { get; set; }
        public string Signature { get; set; }
        public string RealName { get; set; }
        public string Email { get; set; }
    }
}