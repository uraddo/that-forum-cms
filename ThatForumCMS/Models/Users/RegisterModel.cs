﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ThatForumCMS.Models.Users {
    public class RegisterModel {
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Passwords doesn't match")]
        [Display(Name = "Repeat password")]
        public string ConfirmPassword { get; set; }
        
        //[Required]
        [Display(Name = "Your name(optional)")]
        public string RealName { get; set; }

        [Required]
        public string Login { get; set; }

    }
}