﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThatForumCMS.Models {
    public class CategoryIndexViewModel {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<CategoryIndexViewModel> ChildCategories { get; set; }
        public IEnumerable<TopicBadgeViewModel> Topics { get; set; }
    }
}