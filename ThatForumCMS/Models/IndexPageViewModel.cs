﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThatForumCMS.Models {
    public class IndexPageViewModel {
        public IEnumerable<CategoryIndexViewModel> CategoryIndexViewModels { get; set; }
        public string ForumInfo { get; set; }
        // TODO add forum statistics info
    }
}