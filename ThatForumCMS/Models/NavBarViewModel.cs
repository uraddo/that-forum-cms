﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThatForumCMS.Models {
    public class NavBarViewModel {
        public bool IsAdmin { get; set; }
        public bool IsModerator { get; set; }
    }
}