﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThatForumCMS.Models {

    public class TagViewModel {

        public int Id { get; set; }
        public string Name{ get; set; }
        public int ForumId { get; set; }
    }
}
