﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ThatForumCMS.Util;

namespace ThatForumCMS.Models.ErrorLog {
    public class ErrorLogList {
        public IEnumerable<ErrorLogViewModel> ErrorLogs { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}