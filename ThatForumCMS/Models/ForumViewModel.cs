﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThatForumCMS.Models {
    public class TopicViewModel {
        
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Author { get; set; }
        public string Title { get; set; }
        public int ViewsCount { get; set; }
        public int State { get; set; }
        public string TopicMessage { get; set; }
    }
}
