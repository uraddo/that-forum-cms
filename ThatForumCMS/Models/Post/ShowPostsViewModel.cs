﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ThatForumCMS.Util;

namespace ThatForumCMS.Models.Post {
    public class ShowPostsViewModel {
        public IEnumerable<PostViewModel> Posts { get; set; }
        public PagingInfo PagingInfo { get; set; }
        public bool CanUserReportPost { get; set; }
        public bool CanUserDeletePost { get; set; }
        public bool CanUserEditPost { get; set; }
    }
}