﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThatForumCMS.Models.Post {
    public class DeletePostViewModel {
        public int Id { get; set; }
        public string Message { get; set; }
        public int TopicId { get; set; }
        /// <summary>
        /// Can't delete primary post.
        /// In progress - user with lower role can't delete higher role user's post
        /// </summary>
        public bool CanDelete { get; set; } = true;
    }
}