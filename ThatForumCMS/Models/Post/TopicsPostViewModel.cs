﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThatForumCMS.Models.Post {
    public class TopicsPostViewModel {
        public int Id { get; set; }
        public int UserProfileId { get; set; }
        public string Author { get; set; }
        public string Message { get; set; }
        public DateTime PostTime { get; set; }
        public string AvatarUrl { get; set; }
        public string Signature { get; set; }
        public int TotalPostsCount { get; set; }
    }
}