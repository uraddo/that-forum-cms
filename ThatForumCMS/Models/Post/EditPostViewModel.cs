﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThatForumCMS.Models.Post {
    public class EditPostViewModel {
        public int Id { get; set; }
        public string Message { get; set; }
    }
}