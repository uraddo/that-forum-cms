﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThatForumCMS.Models {
    public class PostViewModel {

        public int Id { get; set; }
        public int UserProfileId { get; set; }
        public string Author { get; set; }
        public string Message { get; set; }
        public DateTime PostTime { get; set; }
        public int StatusId { get; set; }

    }
}
