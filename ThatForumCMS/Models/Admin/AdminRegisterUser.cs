﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ThatForumCMS.Infrastructure;
using ThatForumCMS.Models.Users;

namespace ThatForumCMS.Models.Admin {
    public class AdminRegisterUser : RegisterModel {
        public ForumUserRoles Role { get; set; }
    }
}