﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ThatForumCMS.Infrastructure;

namespace ThatForumCMS.Models.Admin {
    public class ManipulateUserViewModel {
        public string Id { get; set; }
        public string Email { get; set; }
        /// <summary>
        /// User's Login name
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// User can state his real name
        /// </summary>
        public string RealName { get; set; }
        public ForumUserRoles Role { get; set; }
    }
}