﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ThatForumCMS.Util;

namespace ThatForumCMS.Models.Admin {
    public class UserInfoListViewModel {
        public IEnumerable<AdminUserInfoViewModel> Users { get; set; }
        public PagingInfo PagingInfo { get; set; }

    }
}