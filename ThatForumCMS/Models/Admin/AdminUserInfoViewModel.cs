﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ThatForumCMS.Infrastructure;

namespace ThatForumCMS.Models.Admin {
    /// <summary>
    /// Used in admin panel - show all users
    /// </summary>
    public class AdminUserInfoViewModel {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        //public ForumUserRoles Role { get; set; }
        public string Role { get; set; }
    }
}