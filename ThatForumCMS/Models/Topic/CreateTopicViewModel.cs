﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ThatForumCMS.Models.Topic {
    public class CreateTopicViewModel {
        public int Id { get; set; }
        [Required]
        public int CategoryId { get; set; }
        public IEnumerable<SelectListItem> Categories { get; set; }
        [Required]
        public string Title { get; set; }
        // For post
        [Required]
        public string Message { get; set; }
    }
}