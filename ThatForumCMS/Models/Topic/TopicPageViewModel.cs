﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ThatForumCMS.Models.Post;
using ThatForumCMS.Util;

namespace ThatForumCMS.Models.Topic {
    public class TopicPageViewModel {
        public int Id { get; set; }
        public string Title { get; set; }
        public int ViewsCount { get; set; }
        public DateTime CreationDate { get; set; }
        //public string CategoryName { get; set; }
        /// <summary>
        /// Only authorized users can reply in topics
        /// </summary>
        public bool IsAuthenticatedUser { get; set; }

        public PagingInfo PagingInfo { get; set; }
        public IEnumerable<TopicsPostViewModel> Posts { get; set; }
    }
}