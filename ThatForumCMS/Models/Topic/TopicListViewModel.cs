﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ThatForumCMS.Util;

namespace ThatForumCMS.Models.Topic {
    public class TopicListViewModel {
        public IEnumerable<TopicViewModel> Topics { get; set; }
        public PagingInfo PagingInfo { get; set; }
        public bool CanUserReportTopic { get; set; }
        public bool CanUserDeleteTopic { get; set; }
        public bool CanUserEditTopic { get; set; }
    }
}