﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ThatForumCMS.Models.Category {
    public class DeleteCategoryViewModel {
        public int Id { get; set; }
        public string Name { get; set; }
        [Display(Name = "Subcategories number")]
        public int ChildCategoriesNum { get; set; }
        [Display(Name = "Delete also all sub-categories")]
        public bool DeleteAllChildCategories { get; set; }
        /// <summary>
        /// if false, they will be placed directly to the parent category.
        /// if no parent category, there will be error - can't delete posts
        /// </summary>
        [Display(Name = "Delete also all category's topics")]
        public bool DeleteAllCategoryTopics { get; set; }
    }
}