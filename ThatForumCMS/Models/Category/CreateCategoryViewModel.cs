﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ThatForumCMS.Models.Category {
    public class CreateCategoryViewModel {
        public int Id { get; set; }
        //[MinLength(3)]
        [StringLength(100, ErrorMessage = "The category name should be at least 3 characters", MinimumLength = 3)]
        public string Name { get; set; }
        [Display(Name = "Parent Category")]
        public int? ParentCategoryId { get; set; }
        //[Display(Name = "Parent Category")]
        public IEnumerable<SelectListItem> ExistingCategories { get; set; }
    }
}