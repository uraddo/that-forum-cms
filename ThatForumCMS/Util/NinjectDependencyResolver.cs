﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ThatForumCMS.BL.Interfaces;
using ThatForumCMS.BL.Services;

namespace ThatForumCMS.Util {
    public class NinjectDependencyResolver : IDependencyResolver {
        private IKernel kernel;
        public NinjectDependencyResolver(IKernel kernelParam) {
            kernel = kernelParam;
            AddBindings();
        }
        public object GetService(Type serviceType) {
            return kernel.TryGet(serviceType);
        }
        public IEnumerable<object> GetServices(Type serviceType) {
            return kernel.GetAll(serviceType);
        }
        private void AddBindings() {
            //kernel.Bind<IUserService>().To<UserService>();
            kernel.Bind<ITopicService>().To<TopicService>();
            kernel.Bind<IErrorLoggerService>().To<ErrorLoggerService>();
            kernel.Bind<ICategoryService>().To<CategoryService>();
            kernel.Bind<IPostService>().To<PostService>();
            kernel.Bind<IUserProfileService>().To<UserProfileService>();
        }
    }

}