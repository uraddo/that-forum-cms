﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using ThatForumCMS.Models.Users;
using ThatForumCMS.BL.EntitiesDTO;
using ThatForumCMS.Models.Admin;
using ThatForumCMS.Infrastructure;

namespace ThatForumCMS.Util.Mappings {
    public class UserRelatedAutoMapperProfile : Profile {

        public UserRelatedAutoMapperProfile() {

            CreateMap<RegisterModel, UserDTO>().ReverseMap();
            CreateMap<AdminRegisterUser, UserDTO>();
            CreateMap<UserDTO, ManipulateUserViewModel>().ReverseMap();//.ForMember(dest => dest.Role, o => o.MapFrom(src => src.Role));
            CreateMap<UserDTO, ManipulateUserViewModel>();
            CreateMap<UserProfileDTO, PublicUserProfileViewModel> ();
            CreateMap<UserProfileDTO, ManipulateUserViewModel>().ReverseMap();
            CreateMap<UserDTO, AdminUserInfoViewModel>()
                .ForMember(dest => dest.Role, o => o.MapFrom(src => src.Role.ToString()));
            CreateMap<UserProfileDTO, EditUserProfile>();
            CreateMap<UserProfileDTO, UserDTO>();
                
        }
        
    }
}