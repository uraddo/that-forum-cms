﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using ThatForumCMS.Models;
using ThatForumCMS.BL.EntitiesDTO;
using ThatForumCMS.Models.Post;

namespace ThatForumCMS.Util.Mappings {
    public class PostRelatedAutoMapperProfile : Profile {
        public PostRelatedAutoMapperProfile() {
            CreateMap<PostDTO, PostViewModel>();
            CreateMap<PostDTO, EditPostViewModel>();
            CreateMap<PostDTO, DeletePostViewModel>();
            CreateMap<PostDTO, TopicsPostViewModel>();
        }
    }
}