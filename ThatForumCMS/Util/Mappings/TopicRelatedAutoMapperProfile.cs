﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using ThatForumCMS.Models;
using ThatForumCMS.BL.EntitiesDTO;
using ThatForumCMS.Models.Topic;

namespace ThatForumCMS.Util.Mappings {
    public class TopicRelatedAutoMapperProfile : Profile {
        public TopicRelatedAutoMapperProfile() {
            CreateMap<TopicDTO, TopicViewModel>();
            CreateMap<TopicViewModel, UpdateTopicDTO>();
            CreateMap<TopicDTO, TopicBadgeViewModel>();
            CreateMap<CreateTopicViewModel, TopicDTO>().ReverseMap();
            CreateMap<TopicDTO, TopicPageViewModel>();
            CreateMap<CreateTopicViewModel, UpdateTopicDTO>();
            CreateMap<TopicDTO, UpdateTopicDTO>();
        }
    }
}