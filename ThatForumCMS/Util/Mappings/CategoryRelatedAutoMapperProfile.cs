﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ThatForumCMS.BL.EntitiesDTO;
using ThatForumCMS.Models;
using ThatForumCMS.Models.Category;

namespace ThatForumCMS.Util.Mappings {
    public class CategoryRelatedAutoMapperProfile : Profile {
        public CategoryRelatedAutoMapperProfile() {
            CreateMap<CategoryDTO, CategoryListItemViewModel>();
            CreateMap<CategoryDTO, CreateCategoryViewModel>().ReverseMap();
            CreateMap<CategoryDTO, DeleteCategoryViewModel>();
            CreateMap<CategoryDTO, CategoryIndexViewModel>();
            CreateMap<CategoryListItemViewModel, CategoryIndexViewModel>();
            CreateMap<DeleteCategoryDTO, DeleteCategoryViewModel>();
            CreateMap<DeleteCategoryViewModel, DeleteCategoryDTO>().ReverseMap();
        }
    }
}