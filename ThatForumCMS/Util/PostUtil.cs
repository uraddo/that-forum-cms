﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using ThatForumCMS.BL.EntitiesDTO;
using ThatForumCMS.Infrastructure;
using ThatForumCMS.Models;

namespace ThatForumCMS.Util {
    public static class PostUtil {

        public static bool IsInAnyRole(IEnumerable<ForumUserRoles> roles, IPrincipal user) {

            foreach (var item in roles) {
                if (user.IsInRole(item.ToString())) {
                    return true;
                }
            }
            return false;
        }
    }
}