﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ThatForumCMS.BL.EntitiesDTO;
using ThatForumCMS.BL.Interfaces;
using ThatForumCMS.Models;
using AutoMapper;

namespace ThatForumCMS.Util {
    public class CategoryUtils {

        public static IEnumerable<TopicBadgeViewModel> GetTopicBadgeViewModels(ITopicService topicService, IUserService UserService, IPostService postService, int categoryId, int numberToTake) {

            var topicDTOs = topicService.GetTopicsFromCategoryOrderByDate(categoryId, numberToTake);
            var topicBadges = Mapper.Map<IEnumerable<TopicDTO>, List<TopicBadgeViewModel>>(topicDTOs);
            foreach (var item in topicBadges) {
                item.Author = UserService.GetUserNameById(item.Author);
                item.CommentNumber = postService.GetTopicsTotalPostsNumber(item.Id) - 1;
            }

            return topicBadges;
        }

        public static IEnumerable<SelectListItem> GetCategoriesListItems(IEnumerable<CategoryDTO> categoriesDTOs, int selectedID = -1) {
            
            var categs = from item in categoriesDTOs
                         select new SelectListItem
                         {
                             Value = item.Id.ToString(),
                             Text = item.Name,
                             Selected = item.Id == selectedID
                         };
            
            if (categs == null || categs.Count() == 0) {
                return null;
            }
            var selected = (from item in categs
                           where item.Selected == true
                           select item).FirstOrDefault();
            if(selected == null) {
                return new SelectList(categs, "Value", "Text");
            }
            return new SelectList(categs, "Value", "Text", selected.Value);
        }
    }
}