﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ThatForumCMS.BL.Interfaces;
using AutoMapper;
using ThatForumCMS.BL.EntitiesDTO;
using ThatForumCMS.Models.Category;
using ThatForumCMS.Util;
using ThatForumCMS.Infrastructure;
using ThatForumCMS.Models;
using Microsoft.AspNet.Identity.Owin;

namespace ThatForumCMS.Controllers
{
    [AuthorizeInAnyRole(ForumUserRoles.SuperAdmin, ForumUserRoles.Admin)]
    public class CategoryController : Controller
    {
        private ICategoryService categoryService;
        private ITopicService topicService;
        private IPostService postService;
        private int ownTopicsOnPage = 5;
        private int subCategorysTopicsOnPage = 2;
        private IUserService UserService {
            get {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }

        public CategoryController(ICategoryService categoryService, ITopicService topicService, IPostService postService) {
            this.categoryService = categoryService;
            this.topicService = topicService;
            this.postService = postService;
        }


        // GET: Category - show all categories lists
        public ActionResult Index()
        {
            var allCategsDTO = categoryService.GetAllCategories();
            var model = Mapper.Map<IEnumerable<CategoryDTO>, List<CategoryListItemViewModel>>(allCategsDTO);
            
            return View(model);
        }

        // GET: Category/View/5
        [AllowAnonymous]
        public ActionResult View(int? id)
        {
            string noCategMsg = "No such category exists";
            if (!id.HasValue) {
                return new HttpStatusCodeResult(404, noCategMsg);
            }
            var entityID = id.Value;


            var categ = categoryService.GetCategoryById(entityID);
            if (categ == null) {
                return new HttpStatusCodeResult(404, "No such category exists");
            }

            var childCategDTOs = categoryService.GetChildCategories(entityID);

            CategoryIndexViewModel categoryViewModels = new CategoryIndexViewModel();
            categoryViewModels.Id = entityID;
            categoryViewModels.Name = categ.Name;
            categoryViewModels.Topics = CategoryUtils.GetTopicBadgeViewModels(topicService, UserService, postService, entityID, ownTopicsOnPage);

            // for each category get subcategories and their own topics
            var subCategories = categoryService.GetChildCategories(entityID);
            var tempSubCategoryIndexVMs = new List<CategoryIndexViewModel>();
            foreach (var subCategory in subCategories) {
                var subCategoryViewModels = new CategoryIndexViewModel();
                subCategoryViewModels = Mapper.Map<CategoryDTO, CategoryIndexViewModel>(subCategory);
                subCategoryViewModels.Topics = CategoryUtils.GetTopicBadgeViewModels(topicService, UserService, postService, subCategory.Id, subCategorysTopicsOnPage);

                tempSubCategoryIndexVMs.Add(subCategoryViewModels);
            }
            categoryViewModels.ChildCategories = tempSubCategoryIndexVMs;


            return View(categoryViewModels);
        }

        // GET: Category/Create
        public ActionResult Create()
        {
            var model = new CreateCategoryViewModel();
            CategoryUtils util = new CategoryUtils();
            var categoriesDTOs = categoryService.GetAllParentCategories();
            model.ExistingCategories = CategoryUtils.GetCategoriesListItems(categoriesDTOs);

            return View(model);
        }

        // POST: Category/Create
        [HttpPost]
        public ActionResult Create(CreateCategoryViewModel model)
        {
            try
            {
                if (ModelState.IsValid) {
                    var categDTO = Mapper.Map<CreateCategoryViewModel, CategoryDTO>(model);
                    categoryService.Create(categDTO);
                } else {
                    return View();
                }

                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("Error", ex);
                return View();
            }
        }

        // GET: Category/Edit/5
        public ActionResult Edit(int? id)
        {
            string noCategMsg = "No such category exists";
            if (!id.HasValue) {
                return new HttpStatusCodeResult(404, noCategMsg);
            }
            var entityID = id.Value;

            CreateCategoryViewModel model;
            try {
                var categoryDTO = categoryService.GetCategoryById(entityID);
                model = Mapper.Map<CategoryDTO, CreateCategoryViewModel>(categoryDTO);
                var categoriesDTOs = categoryService.GetAllParentCategories().Where(c => c.Id != entityID);
                model.ExistingCategories = CategoryUtils.GetCategoriesListItems(categoriesDTOs);
            }
            catch (Exception) {
                return new HttpStatusCodeResult(500, "Something went wrong");
            }
            

            return View(model);
        }

        // POST: Category/Edit/5
        [HttpPost]
        public ActionResult Edit(CreateCategoryViewModel model)
        {
            try {
                if (ModelState.IsValid) {
                    var categDTO = Mapper.Map<CreateCategoryViewModel, CategoryDTO>(model);
                    categoryService.Update(categDTO);
                } else {
                    
                    return View(model);
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex) {
                ModelState.AddModelError("Error", ex);
                return View(model);
            }
        }


        [HttpPost]
        public string EditAjax(CreateCategoryViewModel model) {
            try {
                if (ModelState.IsValid) {
                    if (string.IsNullOrWhiteSpace(model.Name)) {
                        return "The category name can't be empty";
                    }
                    var categDTO = Mapper.Map<CreateCategoryViewModel, CategoryDTO>(model);
                    categoryService.Update(categDTO);
                } else {
                    return "The data you entered isn't valid. " + ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0)
                           .ToList().FirstOrDefault().FirstOrDefault().ErrorMessage;
                }

                return "Done!";
            }
            catch (Exception ex) {
                return "The data you entered isn't valid.";
            }
        }

        // GET: Category/Delete/5
        public ActionResult Delete(int? id)
        {
            string noCategMsg = "No such category exists";
            if (!id.HasValue) {
                return new HttpStatusCodeResult(404, noCategMsg);
            }
            var entityID = id.Value;

            try {
                var categDTO = categoryService.GetCategoryInfoForDeletion(entityID);
                var model = Mapper.Map<DeleteCategoryDTO, DeleteCategoryViewModel>(categDTO);

                return View(model);
            }
            catch (Exception) {
                return new HttpStatusCodeResult(500, "Something went wrong");
            }
            
        }

        // POST: Category/Delete/5
        [HttpPost]
        public ActionResult Delete(DeleteCategoryViewModel model)
        {
            try
            {
                if (ModelState.IsValid) {
                    var deleteCategoryDTO = Mapper.Map<DeleteCategoryViewModel, DeleteCategoryDTO>(model);

                    int childCategoriesNum = categoryService.GetCategoryChildNumber(model.Id);
                    if (childCategoriesNum > 0 && !model.DeleteAllChildCategories) {
                        ModelState.AddModelError("Error", "Can't delete category while child categories exists. Move child categories " +
                            "to other parent category, or make them parent category. But in case if you want to delete also all " +
                            "child categories set corresponding checkbox");
                        model.ChildCategoriesNum = childCategoriesNum;
                        return View(model);
                    }

                    categoryService.Delete(deleteCategoryDTO);//also deletes child categories if correcponding field is set to true

                } else {
                    return View(model);
                }
                
                return RedirectToAction("Index");
            }
            catch
            {
                throw;
                //return View();
            }
        }
        
    }
}
