﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ThatForumCMS.BL.EntitiesDTO;
using ThatForumCMS.BL.Infrastructure;
using ThatForumCMS.BL.Interfaces;
using AutoMapper;
using ThatForumCMS.Models.Users;
using System.IO;
using Microsoft.AspNet.Identity;
using System.Drawing;
using System.Drawing.Drawing2D;
using ThatForumCMS.Models;

namespace ThatForumCMS.Controllers
{
    public class AccountController : Controller
    {
        private IErrorLoggerService errorLogger;
        private IUserProfileService userProfileService;
        private IPostService postService;
        private IUserService UserService {
            get {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }
        private IAuthenticationManager AuthenticationManager {
            get {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        public AccountController(IErrorLoggerService errorLogger, IUserProfileService userProfileService, IPostService postService) {
            this.errorLogger = errorLogger;
            this.userProfileService = userProfileService;
            this.postService = postService;
        }

        // GET: Account
        /// <summary>
        /// If User signed in, redirect to Public User Profile page
        /// </summary>
        [Authorize]
        public ActionResult Index()
        {
            var userLogin = AuthenticationManager.User.Identity.Name;
            if (string.IsNullOrEmpty(userLogin)) {
                errorLogger.Log("Not authenticated user tried to visit the page", url: "/Account/");
            }
            return RedirectToAction("UserProfile", new { user = userLogin });
        }

        /// <summary>
        /// Public User Profile
        /// </summary>
        /// <param name="user">UserName(Login)</param>
        public ActionResult UserProfile(string user) {

            if (string.IsNullOrEmpty(user)) {
                return new HttpStatusCodeResult(404, "No such user exists");
            }

            var userDTO = UserService.GetUserProfileByIdentityName(user);
            if (userDTO == null) {
                return new HttpStatusCodeResult(404, "No such user exists");
            }
            var userProfileViewModel = Mapper.Map<UserProfileDTO, PublicUserProfileViewModel>(userDTO);

            return View(userProfileViewModel);
        }

        public class AvatarVM {
            public string Url { get; set; }
        }

        [HttpPost]
        public PartialViewResult CreateAvatar(string upload) {
            var a = new AvatarVM();
            a.Url = upload;
            return PartialView("_patientList", a);
        }


        [Authorize]
        /// <summary>
        /// Authorized user can change his public profile
        /// </summary>
        public ActionResult Edit() {

            if (TempData["ModelState"] is ModelStateDictionary) {
                ModelState.Merge((ModelStateDictionary)TempData["ModelState"]);
            }

            EditUserProfile model = new EditUserProfile();
            var userProfile = UserService.GetUserProfileByIdentityId(User.Identity.GetUserId());
            model = Mapper.Map<UserProfileDTO, EditUserProfile>(userProfile);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public string ChangeUserPublicInfo(EditUserProfile model) {
            if (ModelState.IsValid) {
                var userProfileDTO = UserService.GetUserProfileByIdentityId(User.Identity.GetUserId());

                try {
                    userProfileService.SetUserProfileAbout(userProfileDTO.Id, model.About);
                    userProfileService.SetUserProfileSignature(userProfileDTO.Id, model.Signature);
                }
                catch (Exception ex) {
                    throw;
                }


                userProfileDTO.RealName = model.RealName;
                userProfileDTO.Email = model.Email;
                try {
                    var usr = Mapper.Map<UserProfileDTO, UserDTO>(userProfileDTO);
                    usr.Id = User.Identity.GetUserId();

                    UserService.Update(usr);
                }
                catch (Exception ex) {

                    throw;
                }
            }

            return "Done!";
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit(EditUserProfile model) {
        //    if (ModelState.IsValid) {
        //        var userProfileDTO = UserService.GetUserProfileByIdentityId(User.Identity.GetUserId());

        //        try {
        //            userProfileService.SetUserProfileAbout(userProfileDTO.Id, model.About);
        //            userProfileService.SetUserProfileSignature(userProfileDTO.Id, model.Signature);
        //        }
        //        catch (Exception ex) {
        //            throw;
        //        }
                

        //        userProfileDTO.RealName = model.RealName;
        //        userProfileDTO.Email = model.Email;
        //        try {
        //            var usr = Mapper.Map<UserProfileDTO, UserDTO>(userProfileDTO);
        //            usr.Id = User.Identity.GetUserId();

        //            UserService.Update(usr);
        //        }
        //        catch (Exception ex) {

        //            throw;
        //        }
        //    }

        //    return RedirectToAction("Edit");
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeAvatar(HttpPostedFileBase upload) {
            if (upload != null) {
                
                // check if is image
                string[] imgFormats = new string[] { ".jpg", ".png", ".gif", ".jpeg" };
                if (!upload.ContentType.Contains("image") 
                    || !imgFormats.Any(item => upload.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase))) {

                    ModelState.AddModelError("AvatarUrl", "Not an image");
                    TempData["ModelState"] = ModelState;
                    return RedirectToAction("Edit");
                }


                int appropriateKBiteSize = 150;
                if (upload.ContentLength > appropriateKBiteSize * 1024) {
                    ModelState.AddModelError("AvatarUrl", "Avatar can't be bigger then " + appropriateKBiteSize + "kb");
                    TempData["ModelState"] = ModelState;

                    return RedirectToAction("Edit");
                }
                using (var img = Image.FromStream(upload.InputStream, true, true)) {
                    if (img.Width != img.Height || img.Width > 300 || img.Height > 300) { 
                        ModelState.AddModelError("AvatarUrl", "Avatar should be a square image, smaller then 300x300px");
                        TempData["ModelState"] = ModelState;
                        return RedirectToAction("Edit");
                    }
                }

                string res = imgFormats.Where(i => upload.FileName.EndsWith(i, StringComparison.OrdinalIgnoreCase)).SingleOrDefault();
                string fileName = DateTime.Now.ToString("yyyyMMddHHmmssfff") + res;//System.IO.Path.GetFileName(upload.FileName);
                int folder;
                try {
                    folder = UserService.GetUserProfileIdByIdentityId(User.Identity.GetUserId());
                }
                catch (Exception) {
                    ModelState.AddModelError("AvatarUrl", "Something went wrong");
                    TempData["ModelState"] = ModelState;
                    return RedirectToAction("Edit");
                }

                string path = "/Content/Avatars/" + folder + "/";
                
                Directory.CreateDirectory(Server.MapPath(path));
                string address = Server.MapPath(path + fileName);
                upload.SaveAs(address);

                address = @"\" + address.Replace(Request.PhysicalApplicationPath, String.Empty);
                userProfileService.SetAvatar(UserService.GetUserProfileByIdentityId(User.Identity.GetUserId()).Id, address);

                return RedirectToAction("Edit");
            }
            return View();
        }

        public ActionResult Login() {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl) {
            await SetInitialDataAsync();
            if (ModelState.IsValid) {
                UserDTO userDto = new UserDTO { Login = model.Login, Password = model.Password };
                ClaimsIdentity claim = await UserService.Authenticate(userDto);
                if (claim == null) {
                    ModelState.AddModelError("", "Login or password is wrong");
                } else {
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = model.RemebmerMe// if true - save Authentication data even after browser closed
                    }, claim);

                    UserService.OnSignedIn(model.Login);

                    // if user being unauthorized tried to visit protected page was redirected to login form, redirect him back to the ogirin page
                    if (!string.IsNullOrEmpty(returnUrl)) {
                        ViewBag.ReturnUrl = returnUrl;
                        return View();
                    }

                    return RedirectToAction("Index", "Home");
                }
            }
            return View(model);
        }


        public ActionResult Logout() {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }


        public ActionResult Register() {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterModel model) {

            //await SetInitialDataAsync();
            if (ModelState.IsValid) {

                var userDto = Mapper.Map<RegisterModel, UserDTO>(model);
                userDto.Role = ForumUserRolesDTO.User;

                OperationDetails operationDetails = await UserService.Create(userDto);

                if (operationDetails.Succedeed) {
                    return View("RegistrationSuccess");
                } else {
                    ModelState.AddModelError(operationDetails.Property, operationDetails.Message);
                }
            }
            return View(model);
        }


        private async Task SetInitialDataAsync() {

            await UserService.SetInitialData(new UserDTO
            {
                Email = "admin@example.com",
                Login = "TheAdmin",
                Password = "111111",
                RealName = "Vasya Pupkin",
                Role = ForumUserRolesDTO.SuperAdmin,
            }, new List<string> {ForumUserRolesDTO.Guest.ToString(),
                ForumUserRolesDTO.User.ToString(),
                ForumUserRolesDTO.Admin.ToString(),
                ForumUserRolesDTO.TrustedModerator.ToString(),
                ForumUserRolesDTO.Moderator.ToString(),
                ForumUserRolesDTO.SuperAdmin.ToString() });
        }



        [HttpPost]
        public async Task<string> ChangePassword(ChangePasswordViewModel model) {

            if (!ModelState.IsValid) {
                return "Error. " + ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).FirstOrDefault().FirstOrDefault().ErrorMessage;
            }

            try {
                var result = await UserService.ChangePassword(User.Identity.GetUserId(), model.CurrentPassword, model.ConfirmPassword);
                if (!result.Succedeed) {
                    return "Error! " + result.Message;
                }

                return "Done!";
            }
            catch (Exception ex) {
                errorLogger.Log("Chagen Pass error", 4, exception: ex);
                return "Something went wrong.";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">UserProfileId</param>
        /// <returns></returns>
        //[HttpPost]
        public PartialViewResult GetLastUsersTopic(int id) {

            var recentPostDTOs = postService.GetRecentPostsFromUser(id, 5, skip: null);
            var recentPosts = Mapper.Map<IEnumerable<PostDTO>, IEnumerable<PostViewModel>>(recentPostDTOs);

            return PartialView(recentPosts);
        }
    }
}