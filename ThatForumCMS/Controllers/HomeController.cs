﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ThatForumCMS.BL;
using AutoMapper;
using ThatForumCMS.Models;
using ThatForumCMS.BL.EntitiesDTO;
using ThatForumCMS.BL.Interfaces;
using ThatForumCMS.Models.Topic;
using ThatForumCMS.Util;
using ThatForumCMS.Models.Category;
using Microsoft.AspNet.Identity.Owin;
using ThatForumCMS.Infrastructure;

namespace ThatForumCMS.Controllers
{

    public class HomeController : Controller
    {
        private ITopicService topicService;
        private IPostService postService;
        private ICategoryService categoryService;
        private int parentCategoryOwnTopicsNumberToShow = 3;
        private int subCategoryOwnTopicsNumberToShow = 2;
        private IUserService UserService {
            get {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }

        public HomeController(ITopicService topicService, ICategoryService categoryService, IPostService postService) {
            this.categoryService = categoryService;
            this.topicService = topicService;
            this.postService = postService;
        }


        // GET: Home
        /// <summary>
        /// Main forum page - show categories, their topics and subcategories with their topics
        /// </summary>
        public ActionResult Index()
        {

            var allParentCategoriesDTO = categoryService.GetAllParentCategories();
            if(allParentCategoriesDTO == null) {
                return View();
            }

            var frontPage = new IndexPageViewModel
            {
                ForumInfo = "Welcome, to some random forum",
                CategoryIndexViewModels = new List<CategoryIndexViewModel>(),
            };

            var allParentCategories = Mapper.Map<IEnumerable<CategoryDTO>, List<CategoryListItemViewModel>>(allParentCategoriesDTO);
            var tempCategoryIndexVMs = new List<CategoryIndexViewModel>();
            
            // TODO actually it's better to use recursion over here
            // TODO I make quite a lot of queries here. It should be better to get all the data from service and retrieve it using nav.properties
            foreach (var category in allParentCategories) {
                var categoryViewModels = Mapper.Map<CategoryListItemViewModel, CategoryIndexViewModel>(category);

                // for each category get it's own topics
                categoryViewModels.Topics = 
                    CategoryUtils.GetTopicBadgeViewModels(topicService, UserService, postService, category.Id, parentCategoryOwnTopicsNumberToShow);

                // for each category get subcategories and their own topics
                var subCategories = categoryService.GetChildCategories(category.Id);
                var tempSubCategoryIndexVMs = new List<CategoryIndexViewModel>();
                foreach (var subCategory in subCategories) {
                    var subCategoryViewModels = new CategoryIndexViewModel();
                    subCategoryViewModels = Mapper.Map<CategoryDTO, CategoryIndexViewModel>(subCategory);
                    subCategoryViewModels.Topics = 
                        CategoryUtils.GetTopicBadgeViewModels(topicService, UserService, postService, subCategory.Id, subCategoryOwnTopicsNumberToShow);


                    tempSubCategoryIndexVMs.Add(subCategoryViewModels);
                }
                categoryViewModels.ChildCategories = tempSubCategoryIndexVMs;

                
                tempCategoryIndexVMs.Add(categoryViewModels);
            }
            frontPage.CategoryIndexViewModels = tempCategoryIndexVMs;
            

            return View(frontPage);
        }


        // GET: Create (Topic)
        [Authorize]
        public ActionResult Create() {

            var createTopicVM = new CreateTopicViewModel();

            // Get All Categories for category drop down list
            var categoriesDTOs = categoryService.GetAllCategories();
            createTopicVM.Categories = CategoryUtils.GetCategoriesListItems(categoriesDTOs);
            
            return View(createTopicVM);
        }


        [ChildActionOnly]
        public PartialViewResult NavBar() {

            var navBarVM = new NavBarViewModel();

            var acceptedAdminRoles = new List<ForumUserRoles>
            {
                ForumUserRoles.SuperAdmin,
                ForumUserRoles.Admin
            };
            navBarVM.IsAdmin = PostUtil.IsInAnyRole(acceptedAdminRoles, User);


            var acceptedModeratorRoles = new List<ForumUserRoles>
            {
                ForumUserRoles.TrustedModerator,
                ForumUserRoles.Moderator
            };
            navBarVM.IsModerator = PostUtil.IsInAnyRole(acceptedModeratorRoles, User);


            return PartialView("AdminNavMenu", navBarVM);
        }
    }
}