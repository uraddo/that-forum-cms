﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ThatForumCMS.BL.Interfaces;
using ThatForumCMS.Models.Topic;
using ThatForumCMS.Util;
using AutoMapper;
using ThatForumCMS.BL.EntitiesDTO;
using ThatForumCMS.Infrastructure;
using ThatForumCMS.Models;
using System.Drawing;
using System.Drawing.Drawing2D;
using ThatForumCMS.Models.Post;

namespace ThatForumCMS.Controllers
{
    //[Authorize]
    public class TopicController : Controller
    {
        #region Properties and fields
        private ICategoryService categoryService;
        private ITopicService topicService;
        private IPostService postService;
        private IUserProfileService userProfileService;
        private IErrorLoggerService errorLogger;
        private int postsPerPage = 3;
        private int topicsPerPageAtModerationPage = 3;
        private IUserService UserService {
            get {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }
        private IAuthenticationManager AuthenticationManager {
            get {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        #endregion

        public TopicController(ITopicService topicService, ICategoryService categoryService, IPostService postService, 
            IErrorLoggerService errorLogger, IUserProfileService userProfileService) {

            this.topicService = topicService;
            this.categoryService = categoryService;
            this.postService = postService;
            this.errorLogger = errorLogger;
            this.userProfileService = userProfileService;
        }

        // GET: Topic
        public ActionResult Index() {
            return RedirectToAction("Index", "Home");
        }

        // GET: Topic/View/5
        public ActionResult View(int? id, int page = 1) {

            string noTopicMsg = "No such topic exists";
            if (!id.HasValue) {
                return new HttpStatusCodeResult(404, noTopicMsg);
            }
            var entityID = id.Value;


            var topicDTO = topicService.GetTopic(entityID);
            if(topicDTO == null) {
                string msg = "No such topic exists";
                //errorLogger.Log(msg, priority: 10, exception: ex);
                return new HttpStatusCodeResult(404, msg);
            }

            var topicPageViewModel = Mapper.Map<TopicDTO, TopicPageViewModel>(topicDTO);
            
            var postDTOs = postService.GetTopicPostsOrderedByDate(entityID, postsPerPage, (page - 1) * postsPerPage);

            if(postDTOs == null) {
                string msg = "Something went wrong";
                errorLogger.Log("Can't get posts in topic", priority: 7, url: "Topic/View/"+ entityID + "?page="+page);
                return new HttpStatusCodeResult(500, msg);
            }

            topicPageViewModel.Posts = Mapper.Map<IEnumerable<PostDTO>, List<TopicsPostViewModel>>(postDTOs);
            foreach (var item in topicPageViewModel.Posts) {

                var user = UserService.GetUserNameByUserProfileId(item.UserProfileId);

                if (string.IsNullOrEmpty(user)) {
                    string msg = "Something went wrong";
                    return new HttpStatusCodeResult(500, msg);
                }
                item.Author = user;
                item.AvatarUrl = userProfileService.GetAvatarUrl(item.UserProfileId);
                item.Signature = userProfileService.GetUserProfileSignature(item.UserProfileId);
            }
            // check if author field filed
            topicPageViewModel.PagingInfo = new PagingInfo
            {
                CurrentPage = page,
                ItemsPerPage = postsPerPage,
                TotalItems = postService.GetTopicsTotalPostsNumber(entityID)
            };

            try {
                topicService.AddViewToTopic(entityID);
            }
            catch (Exception ex) {
                string msg = "Something went wrong";
                errorLogger.Log("Add +1 view topic didn't happened since no such topic exists", priority: 10, exception: ex);
                return new HttpStatusCodeResult(404, msg);
            }

            topicPageViewModel.IsAuthenticatedUser = AuthenticationManager.User.Identity.IsAuthenticated;
            

            return View(topicPageViewModel);
        }

        /// <summary>
        /// Add comment inside topic's page
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult View(int id, string message) {
            if (!AuthenticationManager.User.Identity.IsAuthenticated) {
                return RedirectToAction("Index");
            }
            if (string.IsNullOrEmpty(message)) {
                string msg = "Something went wrong";
                return new HttpStatusCodeResult(500, msg);
            }
            var userProfileDTO = UserService.GetUserProfileByIdentityName(AuthenticationManager.User.Identity.Name);
            if (userProfileDTO == null) {
                string msg = "Something went wrong";
                errorLogger.Log("Authenticated user couldn't be found in DB", priority: 9);
                return new HttpStatusCodeResult(503, msg);
            }
            var topicAuthorUserProfileId = userProfileDTO.Id;

            var postDTO = new PostDTO()
            {
                Message = message,
                PostStatus = PostStatusDTO.Normal,
                TopicId = id,
                PostTime = DateTime.Now,
                UserProfileId = topicAuthorUserProfileId
            };

            try {
                postService.Add(postDTO);
            }
            catch (Exception ex) {
                string msg = "Something went wrong";
                errorLogger.Log(msg, priority: 9, exception: ex);
                return new HttpStatusCodeResult(503, msg);
            }

            return RedirectToAction("View", new { id = id });

        }

        // GET: Topic/Create
        [Authorize]
        public ActionResult Create()
        {
            var createTopicVM = new CreateTopicViewModel();

            // Get All Categories for category drop down list
            var categoriesDTOs = categoryService.GetAllCategories();
            createTopicVM.Categories = CategoryUtils.GetCategoriesListItems(categoriesDTOs);

            return View(createTopicVM);
        }

        // POST: Topic/Create
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateTopicViewModel model)
        {
            try
            {
                if (ModelState.IsValid) {
                    var topicDTO = Mapper.Map<CreateTopicViewModel, TopicDTO>(model);
                    var identityUser = UserService.GetUserProfileByIdentityName(AuthenticationManager.User.Identity.Name);
                    if (identityUser == null) {
                        string msg = "Something went wrong";
                        errorLogger.Log("Authenticated user couldn't be found in DB", priority: 9);
                        return new HttpStatusCodeResult(503, msg);
                    }
                    var topicAuthorUserProfileId = identityUser.Id;

                    var postDTO = new PostDTO()
                    {
                        Message = model.Message,
                        // TODO LATER if this topic from the new user, status -> awaiting moderation
                        // if post has external links(if not from ranked user) - moderation
                        PostStatus = PostStatusDTO.Normal,
                        PostTime = DateTime.Now,
                        UserProfileId = topicAuthorUserProfileId,
                    };
                    
                    var topicId = topicService.AddTopic(topicDTO, postDTO, topicAuthorUserProfileId);
                    return RedirectToAction("View", new { id = topicId });

                } else {
                    return View(model);
                }
            }
            catch(Exception ex)
            {
                string msg = "Something went wrong";
                errorLogger.Log("Authenticated user couldn't be found in DB", exception: ex);
                return new HttpStatusCodeResult(503, msg);
            }
        }


        // GET: Topic/Edit/5
        [AuthorizeInAnyRole(ForumUserRoles.SuperAdmin, ForumUserRoles.Admin, ForumUserRoles.TrustedModerator)]
        public ActionResult Edit(int? id)
        {
            string noTopicMsg = "No such topic exists";
            if (!id.HasValue) {
                return new HttpStatusCodeResult(404, noTopicMsg);
            }
            var entityID = id.Value;


            TopicDTO topicDTO = null;
            PostDTO topicsPrimaryPost = null;

            try {
                topicDTO = topicService.GetTopic(entityID);
                topicsPrimaryPost = postService.GetTopicsPrimaryPost(entityID);
            }
            catch (Exception ex) {
                errorLogger.Log("Error", priority: 9, url: "Topic/Edit", exception: ex);
                return new HttpStatusCodeResult(500, "Something went wrong");
            }
            if (topicDTO == null || topicsPrimaryPost == null) {
                return new HttpStatusCodeResult(404, "No such topic exists");
            }

            var model = Mapper.Map<TopicDTO, CreateTopicViewModel>(topicDTO);
            model.Categories = Util.CategoryUtils.GetCategoriesListItems(categoryService.GetAllCategories(), model.CategoryId);
            model.Message = topicsPrimaryPost.Message;
            
            
            return View(model);
        }

        // POST: Topic/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeInAnyRole(ForumUserRoles.SuperAdmin, ForumUserRoles.Admin, ForumUserRoles.TrustedModerator)]
        public ActionResult Edit(CreateTopicViewModel model)
        {
            try {
                if (ModelState.IsValid) {

                    var updateTopicDTO = Mapper.Map<CreateTopicViewModel, UpdateTopicDTO>(model);
                    topicService.Update(updateTopicDTO);

                } else {
                    return View(model);
                }
                TempData["Success"] = "Topic successfully updated";
                return View();

            }
            catch (Exception ex) {
                string msg = "Something went wrong";
                errorLogger.Log(msg, exception: ex, url: "Topic/Edit");
                return new HttpStatusCodeResult(503, msg);
            }

        }

        // GET: Topic/Delete/5
        [AuthorizeInAnyRole(ForumUserRoles.SuperAdmin, ForumUserRoles.Admin, ForumUserRoles.TrustedModerator)]
        public ActionResult Delete(int? id)
        {
            string noTopicMsg = "No such topic exists";
            if (!id.HasValue) {
                return new HttpStatusCodeResult(404, noTopicMsg);
            }
            var entityID = id.Value;

            TopicDTO topic = null;
            try {
                topic = topicService.GetTopic(entityID);
            }
            catch (Exception ex) {
                errorLogger.Log("Can't get the topic", priority: 9, url: "Topic/Delete", exception: ex);
                return new HttpStatusCodeResult(500, "Something went wrong");
                throw;
            }

            if (topic == null) {
                string msg = "No such topic exists";
                return new HttpStatusCodeResult(404, msg);
            }

            var model = Mapper.Map<TopicDTO, TopicViewModel>(topic);
            PostDTO primaryPost = null;
            try {
                primaryPost = postService.GetTopicsPrimaryPost(model.Id);
            }
            catch (Exception ex) {
                errorLogger.Log("Can't get the topic's primary post", priority: 9, url: "Topic/Delete", exception: ex);
                return new HttpStatusCodeResult(500, "Something went wrong");
            }
            model.TopicMessage = primaryPost != null ? primaryPost.Message : "";

            return View(model);
        }

        // POST: Topic/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeInAnyRole(ForumUserRoles.SuperAdmin, ForumUserRoles.Admin, ForumUserRoles.TrustedModerator)]
        public ActionResult Delete(TopicViewModel model)
        {
            try
            {
                var topicDTO = Mapper.Map<TopicViewModel, UpdateTopicDTO>(model);
                topicService.Delete(topicDTO);
            }
            catch (Exception ex) {
                errorLogger.Log("[Post] Can't delete the topic", priority: 9, url: "Topic/Delete", exception: ex);
                return new HttpStatusCodeResult(500, "Something went wrong");
            }

            return View("DeleteSuccess");
        }

        [AuthorizeInAnyRole(ForumUserRoles.SuperAdmin, ForumUserRoles.Admin, ForumUserRoles.TrustedModerator, ForumUserRoles.Moderator)]
        public ActionResult List(int page = 1) 
        {
            // TODO i perform multiple sql request here, they are not really heavy, but it can be improved as single sql query
            var topicDTOs = topicService.GetLastTopics(topicsPerPageAtModerationPage, (page - 1) * topicsPerPageAtModerationPage);
            var topics = Mapper.Map<IEnumerable<TopicDTO>, List<TopicViewModel>>(topicDTOs);
            foreach (var item in topics) {
                var msg = postService.GetTopicPostsOrderedByDate(item.Id, 1, 0).SingleOrDefault();
                if (msg != null) {
                    item.TopicMessage = msg.Message;
                }                
                item.Author = UserService.GetUserNameById(item.Author);
            }
            var model = new TopicListViewModel
            {
                Topics = topics,
                CanUserDeleteTopic = PostUtil.IsInAnyRole(
                    new List<ForumUserRoles> { ForumUserRoles.Admin, ForumUserRoles.SuperAdmin, ForumUserRoles.TrustedModerator }, User),

                CanUserEditTopic = PostUtil.IsInAnyRole(
                    new List<ForumUserRoles> { ForumUserRoles.Admin, ForumUserRoles.SuperAdmin, ForumUserRoles.TrustedModerator }, User),

                CanUserReportTopic = PostUtil.IsInAnyRole(new List<ForumUserRoles> { ForumUserRoles.Moderator }, User),

                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = topicsPerPageAtModerationPage,
                    TotalItems = topicService.GetAllTopicsCount(),
                }
            };

            return View(model);
        }
        
    }
}
