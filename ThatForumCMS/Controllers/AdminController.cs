﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ThatForumCMS.BL.Interfaces;
using ThatForumCMS.Models.Users;
using AutoMapper;
using ThatForumCMS.BL.EntitiesDTO;
using System.Threading.Tasks;
using ThatForumCMS.Infrastructure;
using ThatForumCMS.Models.Admin;
using ThatForumCMS.Util;
using ThatForumCMS.BL.Infrastructure;
using ThatForumCMS.Models.Post;
using ThatForumCMS.Models;
using ThatForumCMS.Models.ErrorLog;

namespace ThatForumCMS.Controllers
{
    [AuthorizeInAnyRole(ForumUserRoles.SuperAdmin, ForumUserRoles.Admin, ForumUserRoles.TrustedModerator, ForumUserRoles.Moderator)]
    public class AdminController : Controller {

        private IErrorLoggerService errorLogger;
        private ICategoryService categoryService;
        private IPostService postService;
        private int usersPerPage = 20;
        private int postsPerPage = 5;
        private int errorLogsPerPage = 10;

        private IUserService UserService {
            get {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }
        

        public AdminController(IErrorLoggerService errorLogger, ICategoryService categoryService, IPostService postService) {
            this.errorLogger = errorLogger;
            this.categoryService = categoryService;
            this.postService = postService;
        }
        
        
        // GET: Admin
        public ActionResult Index() {
            return View();
        }



        #region User control
        [AuthorizeInAnyRole(ForumUserRoles.SuperAdmin, ForumUserRoles.Admin)]
        public ActionResult AddUser() {

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeInAnyRole(ForumUserRoles.SuperAdmin, ForumUserRoles.Admin)]
        public async Task<ActionResult> AddUser(AdminRegisterUser user) {
            if (ModelState.IsValid) {

                var userDTO = Mapper.Map<AdminRegisterUser, UserDTO>(user);

                var result = await UserService.Create(userDTO);
                if (result.Succedeed) {
                    TempData["Success"] = "User " + userDTO.Login + " added successfully";
                    return RedirectToAction("ShowUsers");
                } else {
                    ModelState.AddModelError("Error", result.Message);
                }
            }

            return View();

        }

        [AuthorizeInAnyRole(ForumUserRoles.SuperAdmin, ForumUserRoles.Admin)]
        public async Task<ActionResult> ShowUsers(int page = 1) {

            IEnumerable<UserDTO> users;
            try {
                users = await UserService.GetAllUsers();
            }
            catch (Exception ex) {
                string msg = "Can't Get All Users";
                errorLogger.Log(msg, priority: 10, exception: ex);
                return new HttpStatusCodeResult(500, msg);
                
            }

            var userInfos = Mapper.Map<IEnumerable<UserDTO>, List<AdminUserInfoViewModel>>(users);
            // TODO refactor this code
            var userInfoList = new UserInfoListViewModel
            {
                Users = userInfos
                    .Skip((page - 1) * usersPerPage)
                    .Take(usersPerPage),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = usersPerPage,
                    TotalItems = userInfos.Count()
                }
            };

            return View(userInfoList);
        }

        [AuthorizeInAnyRole(ForumUserRoles.SuperAdmin, ForumUserRoles.Admin)]
        public ActionResult EditUser(string id) {

            if (string.IsNullOrEmpty(id)) {
                ModelState.AddModelError("Error", "No such user found");
                return View();
            }

            UserProfileDTO user;
            try {
                user = UserService.GetUserProfileByIdentityId(id);
            }
            catch (Exception ex) {
                string msg = "Can't Get User";
                errorLogger.Log(msg, priority: 10, exception: ex);
                return new HttpStatusCodeResult(500, msg);
            }
            ManipulateUserViewModel editUser = Mapper.Map<UserProfileDTO, ManipulateUserViewModel>(user);

            return View(editUser);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeInAnyRole(ForumUserRoles.SuperAdmin, ForumUserRoles.Admin)]
        public async Task<ActionResult> EditUser(ManipulateUserViewModel user) {
            if (ModelState.IsValid) {

                if (string.IsNullOrWhiteSpace(user.Email)) {
                    //ModelState.AddModelError("Error", "Email can't be null or empty");
                    TempData["Result"] = "Email can't be null or empty";
                    return RedirectToAction("EditUser", "Admin",user.Id);
                    //return View("");
                }

                if (string.IsNullOrWhiteSpace(user.Login)) {
                    TempData["Result"] = "Login can't be null or empty";
                    return RedirectToAction("EditUser", "Admin", user.Id);
                }

                var userDTO = Mapper.Map<ManipulateUserViewModel, UserDTO>(user);

                var result = await UserService.Update(userDTO);
                if (result.Succedeed) {
                    TempData["Success"] = "User successfully edited";
                    return RedirectToAction("EditUser", "Admin", user.Id);
                } else {
                    //ModelState.AddModelError("Error", result.Message);
                    TempData["Result"] = result.Message;
                    return RedirectToAction("EditUser", "Admin", user.Id);
                }


            }
            TempData["Result"] = "Something went wrong";
            return RedirectToAction("EditUser", "Admin", user.Id);

        }


        [AuthorizeInAnyRole(ForumUserRoles.SuperAdmin, ForumUserRoles.Admin)]
        public ActionResult DeleteUser(string id) {
            // TODO view where there will be posibility to delete also all user posts and threads

            if (string.IsNullOrEmpty(id)) {
                //ModelState.AddModelError("Error", "No such user found");
                return View();
            }

            UserProfileDTO userProfileDTO;
            try {
                userProfileDTO = UserService.GetUserProfileByIdentityId(id);
            }
            catch (Exception ex) {
                string msg = "Can't Get User";
                errorLogger.Log(msg, priority: 10, exception: ex);
                return new HttpStatusCodeResult(500, msg);
            }
            var user = Mapper.Map<UserProfileDTO, ManipulateUserViewModel>(userProfileDTO);

            return View(user);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeInAnyRole(ForumUserRoles.SuperAdmin, ForumUserRoles.Admin)]
        public async Task<ActionResult> DeleteUser(ManipulateUserViewModel userToDelete) {

            if (string.IsNullOrEmpty(userToDelete.Id)) {
                return new HttpStatusCodeResult(404, "No such user exists");
            }

            OperationDetails operationDetails;
            try {
                operationDetails = await UserService.Delete(userToDelete.Id);
            }
            catch (Exception ex) {
                string msg = "Can't Delete User";
                
                errorLogger.Log(msg, priority: 10, exception: ex);
                return new HttpStatusCodeResult(500, msg);
            }

            if (operationDetails != null && operationDetails.Succedeed) {
                return RedirectToAction("ShowUsers");
            } else {
                ModelState.AddModelError(operationDetails.Property, operationDetails.Message);
                return View();
            }            
        }

        #endregion

        #region Posts control
        
        public ActionResult ShowPosts(int page = 1) {
            // TODO exclude primary posts
            var postDTOs = postService.GetLastPosts(postsPerPage, (page - 1) * postsPerPage);
            var totalPosts = postService.GetTotalPostsNumber();

            var lastPosts = new ShowPostsViewModel
            {
                Posts = Mapper.Map<IEnumerable<PostDTO>, List<PostViewModel>>(postDTOs),

                CanUserDeletePost = PostUtil.IsInAnyRole(
                    new List<ForumUserRoles> { ForumUserRoles.Admin, ForumUserRoles.SuperAdmin, ForumUserRoles.TrustedModerator}, User),

                CanUserEditPost = PostUtil.IsInAnyRole(
                    new List<ForumUserRoles> { ForumUserRoles.Admin, ForumUserRoles.SuperAdmin, ForumUserRoles.TrustedModerator }, User),

                CanUserReportPost = PostUtil.IsInAnyRole(new List<ForumUserRoles> { ForumUserRoles.Moderator}, User),

                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = postsPerPage,
                    TotalItems = totalPosts,
                }
            };
            foreach (var item in lastPosts.Posts) {
                item.Author = UserService.GetUserNameByUserProfileId(item.UserProfileId);
            }
            return View(lastPosts);
        }


        [AuthorizeInAnyRole(ForumUserRoles.SuperAdmin, ForumUserRoles.Admin, ForumUserRoles.TrustedModerator)]
        public ActionResult EditPost(int? id) {
            string noPostMsg = "No such post exists";
            if (!id.HasValue) {
                return new HttpStatusCodeResult(404, noPostMsg);
            }
            var postID = id.Value;

            var post = postService.GetPost(postID);
            if (post == null) {
                return new HttpStatusCodeResult(404, noPostMsg);
            }
            var model = Mapper.Map<PostDTO, EditPostViewModel>(post);


            return View(model);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeInAnyRole(ForumUserRoles.SuperAdmin, ForumUserRoles.Admin, ForumUserRoles.TrustedModerator)]
        public ActionResult EditPost(EditPostViewModel model) {
            
            if (model == null || string.IsNullOrWhiteSpace(model.Message)) {
                ModelState.AddModelError("Message", "Message can't be null or empty");
                return View();
            }
            postService.UpdateMessage(model.Id, model.Message);

            TempData["Success"] = "Post successfully updated";

            return View();
        }


        [AuthorizeInAnyRole(ForumUserRoles.SuperAdmin, ForumUserRoles.Admin, ForumUserRoles.TrustedModerator)]
        public ActionResult DeletePost(int? id) {

            string noPostMsg = "No such post exists";
            if (!id.HasValue) {
                return new HttpStatusCodeResult(404, noPostMsg);
            }
            var postID = id.Value;

            PostDTO postDTO = null;

            try {
                postDTO = postService.GetPost(postID);
            }
            catch (Exception ex) {
                errorLogger.Log("Can't get the post", 9, "/DeletePost", ex);
            }

            if (postDTO == null) {
                return new HttpStatusCodeResult(404, "No such post exists");
            }

            var model = Mapper.Map<PostDTO, DeletePostViewModel>(postDTO);
            model.CanDelete = CanDeletePost(model.TopicId, postID);
            

            return View(model);
        }

        /// <summary>
        /// Check if this post is primary topic's post or [In Progress] user with lower role can't delete higher role user's post
        /// </summary>
        private bool CanDeletePost(int topicId, int id) {
            var topicsPrimaryPost = postService.GetTopicPostsOrderedByDate(topicId, 1, 0).Single();
            if (id == topicsPrimaryPost.Id) {
                return false;
            }
            return true;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeInAnyRole(ForumUserRoles.SuperAdmin, ForumUserRoles.Admin, ForumUserRoles.TrustedModerator)]
        public ActionResult DeletePost(DeletePostViewModel model) {

            if (model == null) {
                ModelState.AddModelError("Error", "Something went wrong");
                return View();
            }
            if (!CanDeletePost(model.TopicId, model.Id)) {
                ModelState.AddModelError("Error", "Can't delete this post");
                return View(model.Id);
            }
            try {
                postService.Delete(model.Id);
            }
            catch(ArgumentException) {
                return new HttpStatusCodeResult(404, "No such post exists");
            }
            catch (Exception ex) {
                errorLogger.Log("Can't remove post", 10, exception: ex);
                TempData["Result"] = "Something went wrong";
                return RedirectToAction("ShowPosts");
            }

            TempData["Success"] = "Post succefully removed";
            return RedirectToAction("ShowPosts");
            
        }
        #endregion

        [AuthorizeInAnyRole(ForumUserRoles.SuperAdmin, ForumUserRoles.Admin)]
        public ActionResult ErrorLogs(int page = 1) {
            var errorLogDTOs = errorLogger.GetErrorLogs(errorLogsPerPage, (page - 1) *  errorLogsPerPage);

            var errorLogVMs = Mapper.Map<IEnumerable<ErrorLogDTO>, List<ErrorLogViewModel>>(errorLogDTOs);

            var errorLogListVM = new ErrorLogList
            {
                ErrorLogs = errorLogVMs,
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = errorLogsPerPage,
                    TotalItems = errorLogger.TotalErrorLogsNumber()
                }
            };

            return View(errorLogListVM);
        }

    }
}