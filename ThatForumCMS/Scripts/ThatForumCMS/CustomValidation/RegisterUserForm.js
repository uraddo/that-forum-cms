﻿$("#regform").validate({
    rules: {
        Password: {
            minlength: 6,
        },
        ConfirmPassword: {
            minlength: 6,
            equalTo: "#Password"
        }
    }
});