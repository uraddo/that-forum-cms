﻿$("#regform").validate({
    rules: {
        CurrentPassword: {
            minlength: 6,
        },
        NewPassword: {
            minlength: 6,
        },
        ConfirmPassword: {
            minlength: 6,
            equalTo: "#NewPassword"
        }
    }
});