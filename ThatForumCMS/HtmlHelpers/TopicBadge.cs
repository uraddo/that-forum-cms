﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ThatForumCMS.Models;

namespace ThatForumCMS.HtmlHelpers {
    public static class TopicBadge {
        public static MvcHtmlString ShowCategoriesTopicsIndex(this HtmlHelper html, IEnumerable<TopicBadgeViewModel> topics) {
            StringBuilder result = new StringBuilder();

            TagBuilder divPanelBody = new TagBuilder("div");
            divPanelBody.AddCssClass("panel-body");

            foreach (var topic in topics) {

                TagBuilder content = new TagBuilder("div");
                content.AddCssClass("panel panel-default");

                TagBuilder topicTitlePanel = new TagBuilder("div");
                topicTitlePanel.AddCssClass("panel-heading");

                TagBuilder topicCategory = new TagBuilder("a");
                topicCategory.MergeAttribute("href", "/Topic/View/" + topic.Id);
                topicCategory.InnerHtml = topic.Title;

                topicTitlePanel.InnerHtml = topicCategory.ToString();
                content.InnerHtml = topicTitlePanel.ToString();

                TagBuilder divTopicPanelBody = new TagBuilder("div");
                divTopicPanelBody.AddCssClass("panel-body");
                TagBuilder linkToAuthor = new TagBuilder("a");
                linkToAuthor.InnerHtml = topic.Author;
                linkToAuthor.MergeAttribute("href", "Profile?user=" + topic.Author);
                divTopicPanelBody.InnerHtml = "Comments number: " + topic.CommentNumber + " | "
                                          + "Created: " + topic.CreationDate.ToString("yyyy.MM.dd HH:mm") + " | "
                                          + "Author: " + linkToAuthor.ToString();

                content.InnerHtml += divTopicPanelBody;
                divPanelBody.InnerHtml += content.ToString();
            }

            //mainDivPanel.InnerHtml += divPanelBody.ToString();

            result.Append(divPanelBody.ToString());
            return MvcHtmlString.Create(result.ToString());
        }
        //public static MvcHtmlString ShowTopicBadges(this HtmlHelper html, IEnumerable<TopicBadgeViewModel> topics) {
        //    StringBuilder result = new StringBuilder();

        //    foreach (var topic in topics) {
        //        TagBuilder tag = new TagBuilder("tr");
        //        TagBuilder tagTD = new TagBuilder("td");
        //        TagBuilder tagDivTitle = new TagBuilder("div");
        //        TagBuilder titleLinkToPost = new TagBuilder("a");
        //        titleLinkToPost.MergeAttribute("href", "/Topic/View/" + topic.Id);
        //        titleLinkToPost.InnerHtml = topic.Title;
        //        tagDivTitle.InnerHtml = titleLinkToPost.ToString();

        //        TagBuilder linkToAuthor = new TagBuilder("a");
        //        linkToAuthor.InnerHtml = topic.Author;
        //        linkToAuthor.MergeAttribute("href", "Profile?user=" + topic.Author);

        //        TagBuilder tagDivOtherInfo = new TagBuilder("div");
        //        tagDivOtherInfo.InnerHtml = "Comments number: " + topic.CommentNumber + " | "
        //            + "Created: " + topic.CreationDate.ToString("yyyy.mm.dd HH:MM") +  " | "
        //            + "Author: " + linkToAuthor.ToString();



        //        tagTD.InnerHtml = tagDivTitle.ToString() + tagDivOtherInfo.ToString();
        //        tag.InnerHtml = tagTD.ToString();

        //        result.Append(tag.ToString());
        //    }

        //    return MvcHtmlString.Create(result.ToString());
        //}

    }
}