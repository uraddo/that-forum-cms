﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ThatForumCMS.Models;
using ThatForumCMS.Util;

namespace ThatForumCMS.HtmlHelpers {
    public static class PagingHelpers {
        public static MvcHtmlString PageLinks(this HtmlHelper html, PagingInfo pagingInfo, Func<int, string> pageUrl) {
            StringBuilder result = new StringBuilder();
            TagBuilder ulTag = new TagBuilder("ul");
            ulTag.AddCssClass("pagination");

            for (int i = 1; i <= pagingInfo.TotalPages; i++) {
                TagBuilder liTag = new TagBuilder("li");

                TagBuilder tag = new TagBuilder("a");
                tag.MergeAttribute("href", pageUrl(i));
                tag.InnerHtml = i.ToString();
                if (i == pagingInfo.CurrentPage) {
                    liTag.AddCssClass("active");
                }
                //tag.AddCssClass("btn btn-default");
                //result.Append(tag.ToString());
                //
                liTag.InnerHtml = tag.ToString();
                ulTag.InnerHtml += liTag.ToString();
            }
            result.Append(ulTag.ToString());

            return MvcHtmlString.Create(result.ToString());
        }
    }
}