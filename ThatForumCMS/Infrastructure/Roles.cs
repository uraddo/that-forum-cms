﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ThatForumCMS.Infrastructure {

    public enum ForumUserRoles {
        Guest = 0,
        User = 1,
        Moderator = 2,
        TrustedModerator = 3,
        Admin = 4,
        SuperAdmin = 5
    }


    /// <summary>
    /// [Deprecated] Due to changes role management - user can be only in one role
    /// </summary>
    public class AuthorizeInAllRoles : AuthorizeAttribute {
        ICollection<ForumUserRoles> rolesList;
        public AuthorizeInAllRoles(params ForumUserRoles[] roles) {
            rolesList = new List<ForumUserRoles>();
            foreach (var item in roles) {
                rolesList.Add(item);
            }
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext) {

            if (httpContext == null) {
                throw new ArgumentNullException("httpContext");
            }

            // Make sure the user is authenticated.
            if (!httpContext.User.Identity.IsAuthenticated) {
                return false;
            }

            foreach (var item in rolesList) {
                //if (!httpContext.User.IsInRole(item.ToRoleString())) {
                if (!httpContext.User.IsInRole(item.ToString())) {
                    return false;
                }
            }

            return true;
        }
    }

    /* TODO DEL this can be simplified to like here:
     * public class RolesAttribute : AuthorizeAttribute
        {
            public RolesAttribute(params string[] roles)
            {
                Roles = String.Join(",", roles);
            }
        }
     */
    public class AuthorizeInAnyRole : AuthorizeAttribute {
        ICollection<ForumUserRoles> rolesList;
        public AuthorizeInAnyRole(params ForumUserRoles[] roles) {
            rolesList = new List<ForumUserRoles>();
            foreach (var item in roles) {
                rolesList.Add(item);
            }
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext) {

            if (httpContext == null) {
                throw new ArgumentNullException("httpContext");
            }

            // Make sure the user is authenticated.
            if (!httpContext.User.Identity.IsAuthenticated) {
                return false;
            }

            foreach (var item in rolesList) {
                if (httpContext.User.IsInRole(item.ToString())) {
                    return true;
                }
            }

            return false;
        }
    }

}
 