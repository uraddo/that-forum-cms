﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using ThatForumCMS.BL.Interfaces;
using ThatForumCMS.BL.Services;

namespace ThatForumCMS.App_Start {
    public partial class Startup {

        IServiceCreator serviceCreator = new ServiceCreator();
        private string nameOrConnectionString = "DefaultConnection";

        public void Configuration(IAppBuilder app) {

            app.CreatePerOwinContext<IUserService>(CreateUserService);

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
            });
        }

        private IUserService CreateUserService() {
            return serviceCreator.CreateUserService(nameOrConnectionString);
        }
    }
}