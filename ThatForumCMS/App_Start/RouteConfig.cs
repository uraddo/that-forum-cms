﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ThatForumCMS
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            #region Account related
            routes.MapRoute(
                name: "UserProf",
                url: "Profile",
                defaults: new { controller = "Account", action = "UserProfile" }
            );
            #endregion

            //routes.MapRoute(
            //    name: "AdminUsers",
            //    url: "Admin/Users/{action}",
            //    defaults: new { controller = "Admin", action = "Index" }
            //);

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
