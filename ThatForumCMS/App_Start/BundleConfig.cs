﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace ThatForumCMS {
    public class BundleConfig {
        public static void RegisterBundles(BundleCollection bundles) {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js"));
            
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            // VALIDATION
            bundles.Add(new ScriptBundle("~/bundles/jqueryvalidation").Include(
                "~/Scripts/jquery.validate*"));
            bundles.Add(new ScriptBundle("~/bundles/validationRegUser").Include(
                "~/Scripts/ThatForumCMS/CustomValidation/RegisterUserForm.js"));
            bundles.Add(new ScriptBundle("~/bundles/validationEditPost").Include(
                "~/Scripts/ThatForumCMS/CustomValidation/EditPost.js"));
            bundles.Add(new ScriptBundle("~/bundles/validationCategoryName").Include(
                "~/Scripts/ThatForumCMS/CustomValidation/CategoryName.js"));
            bundles.Add(new ScriptBundle("~/bundles/validationChangePassword").Include(
                "~/Scripts/ThatForumCMS/CustomValidation/ValidationChangePassword.js"));
            bundles.Add(new ScriptBundle("~/bundles/asyncGetRecentPosts").Include(
                "~/Scripts/ThatForumCMS/AsyncGetRecentPosts.js"));
        }
    }
}