﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ThatForumCMS.BL.EntitiesDTO;
using ThatForumCMS.Models;
using ThatForumCMS.Models.Category;
using ThatForumCMS.Models.ErrorLog;
using ThatForumCMS.Models.Post;
using ThatForumCMS.Models.Topic;
using ThatForumCMS.Util.Mappings;

namespace ThatForumCMS.App_Start {
    public static class MappingConfig {

        public static void Configure() {

            Mapper.Initialize(config =>
            {
                // BL's modules
                config.AddProfile<ThatForumCMS.BL.Mappings.TopicAutoMapperProfile>();
                config.AddProfile<ThatForumCMS.BL.Mappings.UsersAutoMapperProfile>();
                config.AddProfile<ThatForumCMS.BL.Mappings.CategoryAutoMapperProfile>();
                config.AddProfile<ThatForumCMS.BL.Mappings.PostAutoMapperProfile>();
                config.AddProfile<ThatForumCMS.BL.Mappings.ErrorLogAutoMapperProfile>();
                

                // User related mappings
                config.AddProfile<UserRelatedAutoMapperProfile>();

                // Category related mappings
                config.AddProfile<CategoryRelatedAutoMapperProfile>();

                // Topic related
                config.AddProfile<TopicRelatedAutoMapperProfile>();

                // Posts related
                config.AddProfile<PostRelatedAutoMapperProfile>();
                

                // OTHER
                config.CreateMap<TagBL, TagViewModel>();
                config.CreateMap<ErrorLogDTO, ErrorLogViewModel>();
            });

             //Mapper.Configuration.AssertConfigurationIsValid();
        }
    }
}